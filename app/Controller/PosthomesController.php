<?php

class PosthomesController extends AppController {

	public function postar() {

			if( $this->request->is('post') ) {
				
				if ($this->request->data['Posthome']['img']['name'] != NULL){
					$seg = time();	
					$this->Posthome->set(array('imagem' => 'upload/posts/'.$seg.$this->request->data['Posthome']['img']['name']));
					move_uploaded_file($this->data['Posthome']['img']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'posts' . DS .$seg. $this->request->data['Posthome']['img']['name'] );
				}

				if( $this->Posthome->save($this->request->data) ) {
					$this->Posthome->set(array('usuario_id' => $this->Session->read('Usuario.id')));
					$this->Posthome->save();
					$this->redirect($this->referer());
				} else {
					$this->Session->setFlash('Falha ao salvar Post!', 'error');
				}
			}		
	}

	public function excluir($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}

		$this->Posthome->set(array(
			'id' => $id,
			'ativo' => 0
		));

		if( $this->Posthome->save() ) {

			$this->Session->setFlash('Post excluido com sucesso.', 'success');
			$this->redirect($this->referer());
		} else {
			$this->Session->setFlash('Falha ao excluir post!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		//array( 'controller' => 'Tickets', 'action' => 'index' )
	}
	public function postdestaque() {


		if( $this->request->is('post') ) {


			$dest = $this->Posthome->find('first', array( 'conditions' => array( 'Posthome.destaque' => 1, 'Posthome.ativo' => 1 ), 'recursive' => 2 ));

			if($dest != NULL){
				$this->Session->setFlash('Ja existe um post destaque ativo. Por favor, desative o mesmo', 'error');
			}else if( $this->Posthome->save($this->request->data) ) {
					
				if ($this->request->data['Posthome']['img']['name'] != NULL){
					$seg = time();	
					$this->Posthome->set(array('imagem' => 'upload/posts/'.$seg.$this->request->data['Posthome']['img']['name']));
					move_uploaded_file($this->data['Posthome']['img']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'posts' . DS .$seg. $this->request->data['Posthome']['img']['name'] );
				}

					$this->Posthome->set(array('usuario_id' => $this->Session->read('Usuario.id'),
												'destaque' => 1
												));
					$this->Posthome->save();
					$this->redirect(array( 'controller' => 'Pages', 'action' => 'display'));      
				} else {
					$this->Session->setFlash('Falha ao salvar Post!', 'error');
				}
		
		}		
		
	}

}
?>
