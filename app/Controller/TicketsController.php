<?php
App::uses('CakeEmail', 'Network/Email');
class TicketsController extends AppController {

	
	public function index($st = 1) {

		if($this->Session->read('Usuario.email') == NULL) {
		$this->redirect(array( 'controller' => 'Usuarios', 'action' => 'editar', $this->Session->read('Usuario.id')));
		}else{
		$this->loadModel('Departamento');
		$this->set('st',$st);


			if($st == 1){
				if($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'master') ){
					$Ticketsmaster = $this->Ticket->find('all', array( 'conditions' => array( 'Ticket.ativo' => 1), 'recursive' => 1));

					$this->set('tic',$Ticketsmaster);


				}else if ($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply' ) ){
					$Ticketscanreply = $this->Ticket->find('all', array( 'conditions' => array( 'Ticket.ativo' => 1, 'Ticket.destino' => 0), 'recursive' => 1));

					$this->set('tic',$Ticketscanreply);

				}else {
					$Ticketsuser = $this->Ticket->find('all', array( 'conditions' => 
					array( 'Ticket.ativo' => 1, 'Ticket.usuario_id' => $this->Session->read('Usuario.id')), 'recursive' => 1));
					
					$this->set('tic',$Ticketsuser);
				}
			}else{
	
				if ($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){
					$Ticketscanreply = $this->Ticket->find('all', array( 'conditions' => array( 'Ticket.ativo' => 0), 'recursive' => 1));
					
					$this->set('tic',$Ticketscanreply);

				}else {
					$Ticketsuser = $this->Ticket->find('all', array( 'conditions' => 
					array( 'Ticket.ativo' => 0, 'Ticket.usuario_id' => $this->Session->read('Usuario.id')), 'recursive' => 1));
					
					$this->set('tic',$Ticketsuser);
				}
			}

		
		}
	}


	//Validação de dono de ticket

	/*
	if( $this->Departamento->hasPerm($this->Sesssion->read('Usuario.departamento_id') ) {
  	 $this->Departamento->find('all');
		} else {
   $this->Departamento->find('all',array( 'conditions' => array( 'Ticket.usuario_id' => $this->Session->read('Usuario.id'));
}

	$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply'),
	$ticket['Ticket']['usuario_id'] == $this->Session->read('Usuario.id') 

	$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_create_user')

	public function index() {
		$Tickets = $this->Ticket->find('all', array( 'conditions' => array( 'usuario_id' => $this->Session->read('Usuario.id') ), 'recursive' => 1 ));
		$this->set('tic',$Tickets);
	}
	*/






	public function ver($id) {
		$this->loadModel('Resposta');
		$this->loadModel('Departamento');
		/*$this->loadModel('Usuario'); */
		$this->set('ticket',$this->Ticket->find('first',array( 'conditions' => array( 'Ticket.id' => $id ), 'recursive' => 1)));
		/*$this->set('criado',$this->Resposta->find('all', array( 'conditions' => array( 'Usuario.id' => $id ), 'recursive' => 1 )));*/
		$this->set('respostas',$this->Resposta->find('all', array( 'conditions' => array( 'Resposta.ticket_id' => $id, 'Resposta.conclusao' => 0 ), 'recursive' => 1 )));

		$this->set('concluidos',$this->Resposta->find('first', array( 'conditions' => array( 'Resposta.ticket_id' => $id, 'Resposta.conclusao' => 1 ), 'recursive' => 1 )));

		$ticket = $this->Ticket->find('first',array( 'conditions' => array( 'Ticket.id' => $id )));

		if( $ticket['Ticket']['usuario_id'] != $this->Session->read('Usuario.id') && !$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply')) {
		$this->Session->setFlash('Credenciais sem permissão de acesso!', 'error');
		$this->redirect(array( 'controller' => 'Pages', 'action' => 'display', 'home'));
		}
	}
		
			
	

	public function inserir() {
			$this->loadModel('Categoria');
			$this->set('categorias', $this->Categoria->find('list',array('fields'=>array('Categoria.id', 'Categoria.descricao'))));
			if( $this->request->is('post') && !empty($this->request->data)) {

				$this->Ticket->create();
			 // Insere o campo usuario_id no array do formulario, antes de salva-lo
				if ($this->request->data['Ticket']['doc']['name'] != NULL){
					 if ( $this->request->data['Ticket']['doc']['size'] > 10000000) {				
						$this->Session->setFlash('Arquivo maior que 10MB', 'error');
						$this->redirect($this->referer());
						}
						$seg = time();
						$this->Ticket->set(array('documento' => 'upload/documentos/'.$seg.$this->request->data['Ticket']['doc']['name'],
											'usuario_id' => $this->Session->read('Usuario.id')
												));

						$Email = new CakeEmail();
						$Email->config('smtp');
						$Email->emailFormat('html');

						$info = 'Novo Ticket de :'.$this->Session->read('Usuario.nome').'<br><br>'.'Titulo: '.$this->request->data['Ticket']['titulo'].'<br>'.$this->request->data['Ticket']['conteudo'].'<br><br><br>'.'Acesse http://www.miraimotors.com.br/intranet/tickets para vizualizar.';


						if(isset($this->request->data['Ticket']['anonimo'])){
								if($this->request->data['Ticket']['destino']){
									$Email->to('franfreire@miraimotors.com.br');
								}else{
									$Email->to('franfreire@miraimotors.com.br');
									$Email->addTo('rh@miraimotors.com.br');
								}	
						}else{
								if($this->request->data['Ticket']['destino']){
									$Email->to('franfreire@miraimotors.com.br');
									$Email->addTo($this->Session->read('Usuario.email'));
								}else{
									$Email->To('franfreire@miraimotors.com.br');
									$Email->addTo($this->Session->read('Usuario.email'));
									$Email->addTo('rh@miraimotors.com.br');
								}
						}				

						$Email->subject('Mirai Intranet - '.'Novo ticket iniciado :'.$this->request->data['Ticket']['titulo']);

						if($Email->send($info) && $this->Ticket->save($this->request->data) && move_uploaded_file($this->data['Ticket']['doc']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'documentos' . DS .$seg. $this->request->data['Ticket']['doc']['name'] ) ) {
							$this->Session->setFlash('Ticket enviado com sucesso.','success');
			                $this->redirect(array( 'controller' => 'Tickets', 'action' => 'index'));
						} else {
							$this->Session->setFlash('Erro ao enviar ticket!', 'error');
						}
				}else{
						$this->request->data['Ticket']['usuario_id'] = $this->Session->read('Usuario.id'); // Insere o campo usuario_id no array do formulario, antes de salva-lo
						$Email = new CakeEmail();
						$Email->config('smtp');
						$Email->emailFormat('html');

						$info = 'Novo Ticket de :'.$this->Session->read('Usuario.nome').'<br><br>'.'Titulo: '.$this->request->data['Ticket']['titulo'].'<br>'.$this->request->data['Ticket']['conteudo'].'<br><br><br>'.'Acesse http://www.miraimotors.com.br/intranet/tickets para vizualizar.';


						
						if(isset($this->request->data['Ticket']['anonimo'])){
								if($this->request->data['Ticket']['destino']){
									$Email->to('franfreire@miraimotors.com.br');
								}else{
									$Email->to('franfreire@miraimotors.com.br');
									$Email->addTo('rh@miraimotors.com.br');
								}	
						}else{
								if($this->request->data['Ticket']['destino']){
									$Email->to('franfreire@miraimotors.com.br');
									$Email->addTo($this->Session->read('Usuario.email'));
								}else{
									$Email->To('franfreire@miraimotors.com.br');
									$Email->addTo($this->Session->read('Usuario.email'));
									$Email->addTo('rh@miraimotors.com.br');
								}
						}				

						$Email->subject('Mirai Intranet - '.'Novo ticket :'.$this->request->data['Ticket']['titulo']);

						if($Email->send($info) && $this->Ticket->save($this->request->data)){
							$this->Session->setFlash('Ticket enviado com sucesso.', 'success');
							$this->redirect(array( 'controller' => 'Tickets', 'action' => 'index'));
						} else {
							$this->Session->setFlash('Falha ao salvar Ticket!', 'error');
						}
				}
			}
	}

	public function excluir($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}

		$this->Ticket->set(array(
			'id' => $id,
			'status_id' => 3,
			'ativo' => 0
		));

		$this->Ticket->save();

		if( $this->Ticket->save($this->request->data) ) {

			$this->Session->setFlash('Ticket encerrado com sucesso.', 'success');
			$this->redirect(array( 'controller' 	=> 'Tickets', 'action' => 'index'));
		} else {
			$this->Session->setFlash('Falha ao encerrar Ticket!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		//array( 'controller' => 'Tickets', 'action' => 'index' )
	}

}

?>



