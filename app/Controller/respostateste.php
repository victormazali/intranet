<?php

class RespostasController extends AppController {

	public function responder() {
		if( $this->request->is('post')) {
			$this->loadModel('Ticket');

			// Validação de dono do Ticket
			$ticket = $this->Ticket->findById($this->request->data['Resposta']['ticket_id']);
			

			if( $ticket['Ticket']['usuario_id'] == $this->Session->read('Usuario.id')) {

					$this->request->data['Resposta']['usuario_id'] = $this->Session->read('Usuario.id');
					if( $this->Resposta->save($this->request->data)) {
						$this->Ticket->set(array('id' => $this->request->data['Resposta']['ticket_id'], 'status_id' => 1));
						$this->Ticket->save();
						$this->Session->setFlash('Ticket atualizado com sucesso.', 'success');	

					} else {
						$this->Session->setFlash('Falha ao responder ticket.', 'error');
					}

			} else if ($this->Session->read('Usuario.departamento_id') == 1) {

					$this->request->data['Resposta']['usuario_id'] = $this->Session->read('Usuario.id');
					if( $this->Resposta->save($this->request->data)) {
						$this->Ticket->set(array('id' => $this->request->data['Resposta']['ticket_id'], 'status_id' => 2));
						$this->Ticket->save();
						$this->Session->setFlash('Ticket atualizado com sucesso.', 'success');	

					} else {
						$this->Session->setFlash('Falha ao responder ticket.', 'error');
					}

			} 

			} else if{
				$this->Session->setFlash('Falha ao responder ticket.', 'error');
			}

		} else {
			$this->Session->setFlash('Falha ao responder ticket.', 'error');
			$this->redirect($this->referer());	
	}
		$this->redirect(array( 'controller' 	=> 'Tickets', 'action' => 'index'));
}

	
?>
