<?php
App::uses('CakeEmail', 'Network/Email');
class RespostasController extends AppController {

	public function responder() {
		if( $this->request->is('post')) {
			$this->loadModel('Ticket');
			$this->loadModel('Usuario');

			// Validação de dono do Ticket
			$ticket = $this->Ticket->findById($this->request->data['Resposta']['ticket_id']);
			
			$useremail = $this->Ticket->find('first',array( 'conditions' => array( 'Ticket.id' => $this->request->data['Resposta']['ticket_id'] ), 'recursive' => 1));

			//Validação de usuario
			if( $ticket['Ticket']['usuario_id'] == $this->Session->read('Usuario.id')) {
				$this->Resposta->create();
				$this->request->data['Resposta']['usuario_id'] = $this->Session->read('Usuario.id');
					if($this->request->data['Resposta']['doc']['name'] != NULL){
					$seg = time();	
					$this->Resposta->set(array('documento' => 'upload/documentos/'.$seg.$this->request->data['Resposta']['doc']['name']));
					move_uploaded_file($this->data['Resposta']['doc']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'documentos' . DS .$seg. $this->request->data['Resposta']['doc']['name'] );
					}
				if($ticket['Ticket']['anonimo'] == 1){
					$this->Resposta->set(array('anonimo' => 1));
				}	
				$this->Ticket->set(array('id' => $this->request->data['Resposta']['ticket_id'], 'status_id' => 1));
				$this->Ticket->save();
				$this->Resposta->save($this->request->data);

						$Email = new CakeEmail();
						$Email->config('smtp');
						$Email->emailFormat('html');

						$info = 'Movimento de :'.$this->Session->read('Usuario.nome').'<br><br>'.'Titulo: '.$ticket['Ticket']['titulo'].'<br>'.$this->request->data['Resposta']['conteudo'].'<br><br><br>'.'Acesse http://www.miraimotors.com.br/intranet/tickets para vizualizar.';

						if($ticket['Ticket']['anonimo'] == 1){
							if($ticket['Ticket']['destino'] == 1){
								$Email->to('franfreire@miraimotors.com.br');
							}else{
								$Email->To('franfreire@miraimotors.com.br');
								$Email->addTo('rh@miraimotors.com.br');
							}					
						}else{
							if($ticket['Ticket']['destino'] == 1){
								$Email->to('franfreire@miraimotors.com.br');
								$Email->addTo($useremail['Usuario']['email']);
							}else{
								$Email->To('franfreire@miraimotors.com.br');
								$Email->addTo($useremail['Usuario']['email']);
								$Email->addTo('rh@miraimotors.com.br');
							}					
						}


						$Email->subject('Mirai Intranet - '.'Movimento no ticket :'.$ticket['Ticket']['titulo']);
						$Email->send($info);

				$this->Session->setFlash('Ticket atualizado com sucesso.', 'success');	
				$this->redirect(array( 'controller' 	=> 'Tickets', 'action' => 'ver', $ticket['Ticket']['id']));


			//Validação de can_reply	
			}else if( $this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){
				$this->Resposta->create();
				$this->request->data['Resposta']['usuario_id'] = $this->Session->read('Usuario.id');
					if($this->request->data['Resposta']['doc']['name'] != NULL){
					$seg = time();	
					$this->Resposta->set(array('documento' => 'upload/documentos/'.$seg.$this->request->data['Resposta']['doc']['name']));
					move_uploaded_file($this->data['Resposta']['doc']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'documentos' . DS .$seg. $this->request->data['Resposta']['doc']['name'] );
					}
				
				$this->Ticket->set(array('id' => $this->request->data['Resposta']['ticket_id'], 'status_id' => 2));
				$this->Ticket->save();
				$this->Resposta->save($this->request->data);

						$Email = new CakeEmail();
						$Email->config('smtp');
						$Email->emailFormat('html');

						$info = 'Movimento de :'.$this->Session->read('Usuario.nome').'<br><br>'.'Titulo: '.$ticket['Ticket']['titulo'].'<br>'.$this->request->data['Resposta']['conteudo'].'<br><br><br>'.'Acesse http://www.miraimotors.com.br/intranet/tickets para vizualizar.';



						if($ticket['Ticket']['anonimo'] == 1){
							if($ticket['Ticket']['destino'] == 1){
								$Email->to('franfreire@miraimotors.com.br');
							}else{
								$Email->To('franfreire@miraimotors.com.br');
								$Email->addTo('rh@miraimotors.com.br');
							}					
						}else{
							if($ticket['Ticket']['destino'] == 1){
								$Email->to('franfreire@miraimotors.com.br');
								$Email->addTo($useremail['Usuario']['email']);
							}else{
								$Email->To('franfreire@miraimotors.com.br');
								$Email->addTo($useremail['Usuario']['email']);
								$Email->addTo('rh@miraimotors.com.br');
							}					
						}


						$Email->subject('Mirai Intranet - '.'Movimento no ticket :'.$ticket['Ticket']['titulo']);
						$Email->send($info);

				$this->Session->setFlash('Ticket atualizado com sucesso.', 'success');	
				$this->redirect(array( 'controller' 	=> 'Tickets', 'action' => 'ver', $ticket['Ticket']['id']));
			}


		}
	}

	public function concluir() {
		if( $this->request->is('post')) {
			$this->loadModel('Ticket');

				$ticket = $this->Ticket->findById($this->request->data['Resposta']['ticket_id']);

				$this->Resposta->create();

				$this->request->data['Resposta']['usuario_id'] = $this->Session->read('Usuario.id');
					if($this->request->data['Resposta']['doc']['name'] != NULL){
					$seg = time();	
					$this->Resposta->set(array('documento' => 'upload/documentos/'.$seg.$this->request->data['Resposta']['doc']['name']));
					move_uploaded_file($this->data['Resposta']['doc']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'documentos' . DS .$seg. $this->request->data['Resposta']['doc']['name'] );
					}
				if( !$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply')  ) {
					if($ticket['Ticket']['anonimo'] == 1){
						$this->Resposta->set(array('anonimo' => 1));
					}
				}
				$this->Ticket->set(array('id' => $this->request->data['Resposta']['ticket_id'], 'status_id' => 3, 'ativo' => 0));
				$this->Ticket->save();
				$this->Resposta->set(array('conclusao' => 1));
				$this->Resposta->save($this->request->data);

						$Email = new CakeEmail();
						$Email->config('smtp');
						$Email->emailFormat('html');

						$info = 'Encerrado por :'.$this->Session->read('Usuario.nome').'<br><br>'.'Titulo: '.$ticket['Ticket']['titulo'].'<br>'.$this->request->data['Resposta']['conteudo'].'<br><br><br>'.'Acesse http://www.miraimotors.com.br/intranet/tickets para vizualizar.';



						if($ticket['Ticket']['anonimo'] == 1){
							if($ticket['Ticket']['destino'] == 1){
								$Email->to('franfreire@miraimotors.com.br');
							}else{
								$Email->To('franfreire@miraimotors.com.br');
								$Email->addTo('rh@miraimotors.com.br');
							}					
						}else{
							if($ticket['Ticket']['destino'] == 1){
								$Email->to('franfreire@miraimotors.com.br');
								$Email->addTo($useremail['Usuario']['email']);
							}else{
								$Email->To('franfreire@miraimotors.com.br');
								$Email->addTo($useremail['Usuario']['email']);
								$Email->addTo('rh@miraimotors.com.br');
							}					
						}


						$Email->subject('Mirai Intranet - '.'Ticket Encerrado :'.$ticket['Ticket']['titulo']);
						$Email->send($info);
				$this->Session->setFlash('Ticket encerrado com sucesso.', 'success');	
				$this->redirect(array( 'controller' 	=> 'Tickets', 'action' => 'ver', $ticket['Ticket']['id']));


		
		}
	}

}
?>
