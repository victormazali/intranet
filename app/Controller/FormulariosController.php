<?php

class FormulariosController extends AppController {

	public function index(){
		$formularios = $this->Formulario->find('all', array( 'conditions' => array( 'Formulario.ativo' => 0 )));
		$this->set('for',$formularios);
	}

	public function adm($st = 1) {
		$this->set('st',$st);

		if($st == 1){
		$formularios = $this->Formulario->find('all', array( 'conditions' => array( 'Formulario.ativo' => 1 )));
		$this->set('for',$formularios);
		}else{
		$formularios = $this->Formulario->find('all', array( 'conditions' => array( 'Formulario.ativo' => 0 )));
		$this->set('for',$formularios);
		}
	}


	public function inserir(){
		$this->loadModel('FormCategoria');
		$this->set('categorias', $this->FormCategoria->find('list', array( 'conditions' => array( 'FormCategoria.ativo' => 1 ),'fields' => array( 'FormCategoria.id', 'FormCategoria.descricao'))));
		if( $this->request->is('post')) {

		
			if( $this->Formulario->saveAll($this->request->data) ) {
				$this->Session->setFlash('Titulo salvo com sucesso.', 'success');
			} else {
				$this->Session->setFlash('Falha ao salvar formulario!', 'error');
			}
		}

	}
}
?>