<?php

class NotificacoesController extends AppController {

	
	public function index() {
		$this->layout = 'default';
		
		$notificacoes = $this->Notificacoes->find('all', array( 'conditions' => array( 
																			'Notificacoes.ativo' => 1, 
																			'Notificacoes.usuario_id' == $this->Session->read('Usuario.id')
																			), 'recursive' => 1 ));

		$this->set('not', $notificaoes);
}
?>