<?php

class DepartamentosController extends AppController {
	
	public function beforeFilter() {
		if(!$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){
			$this->Session->setFlash('Credencias sem permissão de acesso!', 'error');
			$this->redirect(array( 'controller' => 'Pages', 'action' => 'display', 'home'));
		}
	}
	public function index($st = 1) {
		$this->set('st',$st);

		if($st == 1){
		$departamentos = $this->Departamento->find('all', array( 'conditions' => array( 'ativo' => 1 )));
		$this->set('dps',$departamentos);
	}else{
		$departamentos = $this->Departamento->find('all', array( 'conditions' => array( 'ativo' => 0 )));
		$this->set('dps',$departamentos);
	}

	}

	public function inserir() {
		if( $this->request->is('post') ) {
			if( $this->Departamento->save($this->request->data) ) {	
				$this->Session->setFlash('Departamento salvo com sucesso.', 'success');
				$this->redirect(array( 'controller' => 'Departamentos', 'action' => 'index'));   
			} else {
				$this->Session->setFlash('Falha ao salvar departamento!', 'error');
			}
		}
	}

	public function editar($id) {
		$this->Departamento->id = $id;

		if ($this->request->is('get')) {
			$this->request->data = $this->Departamento->read();
		} else {
			if ( $this->Departamento->save($this->request->data)) {			
				$this->Session->setFlash('Departamento editado com sucesso.','success');
                $this->redirect(array( 'controller' => 'Departamentos', 'action' => 'index'));             
            }
		}
	}

	public function excluir($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}

		$this->Departamento->set(array(
			'id' => $id,
			'ativo' => 0
		));

		if( $this->Departamento->save() ) {
			$this->Session->setFlash('Departamento desativado com sucesso.', 'success');
		} else {
			$this->Session->setFlash('Falha ao desativar departamento!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		$this->redirect($this->referer());
		//array( 'controller' => 'Departamentos', 'action' => 'index' )
	}


}
?>