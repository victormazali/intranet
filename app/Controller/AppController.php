<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public function beforeFilter() {
		if( $this->action != 'login' ) {
			if( $this->Session->check('Usuario.id') == false ) { // Se não tiver na action de login, e sem sessão, redireciona.
				$this->Session->setFlash('Sem permissão de acesso.', 'error');
				$this->redirect(array( 'controller' => 'Usuarios', 'action' => 'login' ));
			} else { // Faz uma nova validação de dados por motivos de segurança
				
				$this->loadModel('Usuario'); // Carrega model do usuário
				$this->loadModel('Departamento');

				$dados = $this->Usuario->find('first',array('recursive' => 1, 
					'conditions' => array( 
						'Usuario.id' => $this->Session->read('Usuario.id'), 
						'Usuario.cpf' => $this->Session->read('Usuario.cpf'), 
						'Usuario.senha' => $this->Session->read('Usuario.senha'),
						'Usuario.nome' => $this->Session->read('Usuario.nome'),
						'Usuario.departamento_id' => $this->Session->read('Usuario.departamento_id'),
						'Usuario.imagem' => $this->Session->read('Usuario.imagem'),
						'Usuario.email' => $this->Session->read('Usuario.email'),
						/*'Departamento.can_reply' => $this->Session->read('Usuario.can_reply'),*/
						)
					)
				);
				
				if( $dados == NULL ) {
					$this->Session->destroy('Usuario');
					$this->Session->setFlash('Dados de acesso inconsistentes, favor realizar acesso novamente.', 'error');
					$this->redirect(array( 'controller' => 'Usuarios', 'action' => 'login' ));
				}
			}
		}
	}
	public function BeforeRender() {
		$this->loadModel('Ticket');
		$this->loadModel('Usuario');
		$this->loadModel('Departamento');
		$this->loadModel('Notificacao');

		$this->set('notificacoes',$this->Notificacao->find('all', array( 'conditions' => array( 'Notificacao.usuario_id' => $this->Session->read('Usuario.id')) )));

		if($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'master') ){
			$admin = 1;
			$this->set('canMaster',$admin);
		} else {
			$user = 0;
			$this->set('canMaster',$user);
		}


		if ($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){
			$admin = 1;
			$this->set('canAdmin',$admin);
		} else {
			$user = 0;
			$this->set('canAdmin',$user);
		}
	}
}
