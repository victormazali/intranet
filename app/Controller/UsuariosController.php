<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class UsuariosController extends AppController {

/*	public function beforeFilter() {

		if( strtolower($this->action) != 'login'
			&& strtolower($this->action) != 'logout'
			&& !$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') 

		)
		{
			$this->Session->setFlash('Credencias sem permissão de acesso!', 'error');
			$this->redirect(array( 'controller' => 'Pages', 'action' => 'display', 'home'));
		}
	}
	*/
	public function index($st = 1) {
		$this->set('st',$st);

		if($st == 1){
		$usuarios = $this->Usuario->find('all', array( 'conditions' => array( 'Usuario.ativo' => 1 ), 'recursive' => 1));
		// construtor de query / parametros
		$this->set('users',$usuarios);
	}else{
		$usuarios = $this->Usuario->find('all', array( 'conditions' => array( 'Usuario.ativo' => 0 ), 'recursive' => 1));
		// construtor de query / parametros
		$this->set('users',$usuarios);
	}

		$this->loadModel('Departamento');
	}

	

	public function inserir() {
		$this->loadModel('Departamento');
		$this->set('departamentos', $this->Departamento->find('list',array('fields'=>array('Departamento.id', 'Departamento.descricao'))));
		if( $this->request->is('post') ) {

			if ($this->request->data['Usuario']['img']['name'] != NULL){
					$ext = substr(strtolower(strrchr($this->request->data['Usuario']['img']['name'], '.')), 1); //get the extension
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'bmp'); //set allowed extensions

					if (!in_array($ext, $arr_ext)) {
					$this->Session->setFlash('Tipo de arquivo não suportado!', 'error');
					$this->redirect($this->referer());
					} else if ( $this->request->data['Usuario']['img']['size'] > 10000000) {				
					$this->Session->setFlash('Arquivo maior que 10MB', 'error');
					$this->redirect($this->referer());
					}
					$seg = time();	
					$this->Usuario->set(array('imagem' => 'upload/perfil/'.$seg.$this->request->data['Usuario']['img']['name']));
					move_uploaded_file($this->data['Usuario']['img']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'perfil' . DS .$seg. $this->request->data['Usuario']['img']['name'] );
				} else {
					$this->Usuario->set(array('imagem' => 'upload/perfil/avatar.png'));
				}

			if( $this->Usuario->save($this->request->data) ) {	
				$this->Session->setFlash('Usuario salvo com sucesso.', 'success');
				$this->redirect ( array('controller' => 'Usuarios', 'action' => 'index') );
			} else {
				$this->Session->setFlash('Falha ao salvar usuario!', 'error');
			}
		}
		
	}

	public function editar($id) {
		$this->Usuario->id = $id;
		$this->loadModel('Departamento');


		if ($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){
				$admin = 1;
				$this->set('canAdmin',$admin);

			}else {
				$user = 0;
				$this->set('canAdmin',$user);
			}
		
		$this->set('user', $this->Usuario->find('first',array('conditions' => array('Usuario.id' => $id), 'recursive' => 1 ) ) );

		$this->set('dp', $this->Departamento->find('list',array('fields'=>array('Departamento.id', 'Departamento.descricao'))));

		if(!$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') && $this->Session->read('Usuario.id') !== $id ){
		$this->Session->setFlash('Credencias sem permissão de acesso!', 'error');
		$this->redirect(array( 'controller' => 'Pages', 'action' => 'display', 'home'));
		}


		if ($this->request->is('get')) {
			$this->request->data = $this->Usuario->read();
		} else {
	
				if ($this->request->data['Usuario']['img']['name'] != NULL){
					$ext = substr(strtolower(strrchr($this->request->data['Usuario']['img']['name'], '.')), 1); //get the extension
					$arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'bmp'); //set allowed extensions

					if (!in_array($ext, $arr_ext)) {
					$this->Session->setFlash('Tipo de arquivo não suportado!', 'error');
					$this->redirect($this->referer());
					} else if ( $this->request->data['Usuario']['img']['size'] > 10000000) {				
					$this->Session->setFlash('Arquivo maior que 10MB', 'error');
					$this->redirect($this->referer());
					}
					$seg = time();	
					$this->Usuario->set(array('imagem' => 'upload/perfil/'.$seg.$this->request->data['Usuario']['img']['name']));
					$this->Usuario->set(array('teste' => 1));
					move_uploaded_file($this->data['Usuario']['img']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'perfil' . DS .$seg. $this->request->data['Usuario']['img']['name'] );
				}
				$this->Usuario->save($this->request->data);
				$this->Session->setFlash('Usuario editado com sucesso.','success');
                $this->redirect(array( 'controller' => 'Pages', 'action' => 'display', 'home'));         
           
		}
	}

	public function alterar($id) {
		$this->Usuario->id = $id;
		$this->loadModel('Departamento');
	

		$this->set('dp',$this->Usuario->find('first',array( 'conditions' => array( 'Usuario.id' => $id ), 'recursive' => 1)));

		if(!$this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') && $this->Session->read('Usuario.id') !== $id ){
		$this->Session->setFlash('Credencias sem permissão de acesso!', 'error');
		$this->redirect(array( 'controller' => 'Pages', 'action' => 'display', 'home'));
		}

		if ($this->request->is('get')) {
			$this->request->data = $this->Usuario->read();
		} else {
			if ( $this->Usuario->save($this->request->data)) {			
				$this->Session->setFlash('Usuario editado com sucesso.','success');
                $this->redirect(array( 'controller' => 'Usuarios', 'action' => 'index'));             
            }
		}
	}

	public function excluir($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}

		$this->Usuario->set(array(
			'id' => $id,
			'ativo' => 0
		));

		if( $this->Usuario->save() ) {
			$this->Session->setFlash('Usuario desativado com sucesso!', 'success');
		} else {
			$this->Session->setFlash('Falha ao desativar usuario!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		$this->redirect($this->referer());
		//array( 'controller' => 'Departamentos', 'action' => 'index' )
	}

	public function login() {
		$this->loadModel('Departamento');

		$this->layout = 'login';
		// Se a requisição é um POST, ou seja, se recebi os dados de um formulário enviado.
		if( $this->request->is('post') ) {
			// echo var_dump($this->request->data); para debugar os dados
			$senha = $this->request->data['Usuario']['senha']; // $this->request->data contém os dados recebidos no POST

			// Criptografa a senha recebida, para comparar com a senha salva no banco
			$passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256')); // Componente do CakePHP
            $senhaCriptografada = $passwordHasher->hash( $senha ); // Chama função de criptografar

			$dados = $this->Usuario->find('first', array( 'conditions' => array( 'Usuario.cpf' => $this->request->data['Usuario']['cpf'], 'Usuario.senha' => $senhaCriptografada )));

			if( $dados != NULL ) { // Usuário existe

				$this->Session->write('Usuario.id',$dados['Usuario']['id']);
				$this->Session->write('Usuario.cpf',$dados['Usuario']['cpf']);
				$this->Session->write('Usuario.senha',$dados['Usuario']['senha']);
				$this->Session->write('Usuario.nome',$dados['Usuario']['nome']);
				$this->Session->write('Usuario.departamento_id',$dados['Usuario']['departamento_id']);
				$this->Session->write('Usuario.imagem',$dados['Usuario']['imagem']);
				$this->Session->write('Usuario.email',$dados['Usuario']['email']);
				/*$this->Session->write('Departamento.can_reply',$dados['Departamento']['can_reply']);*/

				$this->Session->setFlash('Usuário logado com sucesso!', 'success');
				
				$this->redirect(array( 'controller' => 'Pages', 'action' => 'display' ));
			} else { // Usuário não existe
				$this->Session->setFlash('Credenciais de acesso inválidas', 'error');
			}

		}
	}

	public function logout() {
		$this->Session->destroy('Usuario');
		$this->Session->setFlash('Sessão encerrada com sucesso!', 'success');
		$this->redirect(array( 'controller' => 'Usuarios', 'action' => 'login' ));
	}


}
?>