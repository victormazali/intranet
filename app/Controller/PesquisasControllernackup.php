<?php

class PesquisasController extends AppController {

	public function index(){
		$this->loadModel('Estado');
		$clientes = $this->Pesquisa->find('all', array('recursive' => 1));
		$this->set('dps',$clientes);

		$this->set('perguntas1',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta1 !=' => '') ) ) );

		$this->set('perguntas2',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta2 !=' => '') ) ) );

		$this->set('perguntas3',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta3 !=' => '') ) ) );

		$this->set('perguntas4',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta4 !=' => '') ) ) );

		$this->set('perguntas5',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta5 !=' => '') ) ) );

		$this->set('perguntas6',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta6 !=' => '') ) ) );
	
	}

	public function ver($id){
		$this->loadModel('Estado');
		$this->Pesquisa->id = $id;
		$this->set('ticket',$this->Pesquisa->find('first',array( 'conditions' => array( 'Pesquisa.id' => $id ), 'recursive' => 1)));

		if ($this->request->is('get')) {
			$this->request->data = $this->Pesquisa->read();
		}

	}

	public function verperguntas($id){
				
		$this->set('perguntas', $this->Pesquisa->find('all'));

		$this->set('pergunta', $id);
		
		$this->set('perguntas1',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta1 !=' => '') ) ) );

		$this->set('perguntas2',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta2 !=' => '') ) ) );

		$this->set('perguntas3',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta3 !=' => '') ) ) );

		$this->set('perguntas4',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta4 !=' => '') ) ) );

		$this->set('perguntas5',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta5 !=' => '') ) ) );

		$this->set('perguntas6',$this->Pesquisa->find('count',array( 'conditions' => array( 'pergunta6 !=' => '') ) ) );



	}


	public function inserir(){
		if( $this->request->is('post') ) {
			if( $this->Pesquisa->save($this->request->data) ) {	
				$this->Session->setFlash('Pesquisa realizada com sucesso.', 'success');
				$this->redirect(array( 'controller' => 'Pesquisas', 'action' => 'inserir'));   
			} else {
				$this->Session->setFlash('Falha ao realizar pesquisa!', 'error');
			}
		}
	}
}
?>