<?php

class PostsController extends AppController {

	public function postar() {
			$this->loadModel('Evento');

			if( $this->request->is('post') ) {
					
					if ($this->request->data['Post']['img']['name'] != NULL){
					$seg = time();	
					$this->Post->set(array('imagem' => 'upload/posts/'.$seg.$this->request->data['Post']['img']['name']));
					move_uploaded_file($this->data['Post']['img']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'posts' . DS .$seg. $this->request->data['Post']['img']['name'] );
				}

				if( $this->Post->save($this->request->data) ) {

					$this->Post->set(array('usuario_id' => $this->Session->read('Usuario.id')));
					$this->Post->save();
					$this->redirect($this->referer());
					#$this->Post->request->data['Evento']['id']
				} else {
					$this->Session->setFlash('Falha ao salvar Post!', 'error');
				}
			}		
}

}
?>
