<?php
App::uses('CakeEmail', 'Network/Email');

class EventosController extends AppController {

	
	public function index() {
		$this->loadModel('Usuario');
		$this->loadModel('Presenca');
		$this->loadModel('Post');
		
		$eventos = $this->Presenca->find('all', array( 'conditions' => array( 'Evento.ativo' => 1, 'Presenca.usuario_id' => $this->Session->read('Usuario.id')), 'recursive' => 1, 'order' => array('Evento.data' => 'ASC') ));

	
		foreach( $eventos as $key => $evt ) {
			echo $key;
			//$evt['Evento']['posts'] = ;
			$eventos[$key]['Evento']['convidados'] = $this->Presenca->find('count', array( 'conditions' => array( 'evento_id' => $evt['Evento']['id'] )));
			$eventos[$key]['Evento']['posts'] = $this->Post->find('count', array( 'conditions' => array( 'evento_id' => $evt['Evento']['id'] )));
			$eventos[$key]['Evento']['confirmados'] = $this->Presenca->find('count', array( 'conditions' => array( 'evento_id' => $evt['Evento']['id'], 'confirmado' => 1 )));
			$eventos[$key]['Evento']['acompanhantes'] = $this->Presenca->find('count', array( 'conditions' => array( 'evento_id' => $evt['Evento']['id'], 'acompanhante !=' => '' )));
		}
				
		$this->set('evt', $eventos);				


		if( $this->request->is('post') ) {
			$user = $this->Session->read('Usuario.id');
			if( $this->Presenca->save($this->request->data) ) {	
				#$this->Presenca->set('confirmado' => 1));
				#$this->Presenca->set('usuario_id' => $user));
				#$this->Presenca->save();
				$this->Session->setFlash('Presença confirmado com sucesso!', 'success');
				$this->redirect ( array('controller' => 'Eventos', 'action' => 'index') );
			} else {
				$this->Session->setFlash('Falha ao confirmar presença!', 'error');
			}
		}
		
	}

	public function lista($id) {
		$this->loadModel('Usuario');
		$this->loadModel('Presenca');

		$this->set('evento',$this->Evento->find('first',array( 'conditions' => array( 'Evento.id' => $id ), 'recursive' => 1)));

		$this->set('presenca',$this->Presenca->find('all',array( 'conditions' => array( 'evento_id' => $id, 'Presenca.ativo' => 1 ), 'recursive' => 1)));

		$this->set('number',$this->Presenca->find('count',array( 'conditions' => array( 'evento_id' => $id, 'confirmado' => 1, 'Presenca.ativo' => 1 ), 'recursive' => 1)));

		$this->set('naovai',$this->Presenca->find('count',array( 'conditions' => array( 'evento_id' => $id, 'confirmado' => 2, 'Presenca.ativo' => 1 ), 'recursive' => 1)));

		$this->set('acp',$this->Presenca->find('count',array( 'conditions' => array( 'evento_id' => $id, 'Presenca.ativo' => 1, 'acompanhante !=' => ''), 'recursive' => 1)));

		$this->set('conv',$this->Presenca->find('count',array( 'conditions' => array( 'evento_id' => $id, 'Presenca.ativo' => 1 ), 'recursive' => 1)));


		// Verifica se já existe um registro para aquele usuário naquele evento na tabela de Presenças
		$userconfir = $this->Presenca->find('first', array( 'conditions' => array( 'evento_id' => $id, 'usuario_id' => $this->Session->read('Usuario.id') )));
		$jaConfirmou = $userconfir['Presenca']['confirmado'];

		$this->set('jaConfirmou',$userconfir);
		


		if( $this->request->is('post') ) {
			$this->loadModel('Presenca');

			// Se já confirmou, redireciona de volta.
			if( $jaConfirmou == 1 ) {
				$this->Session->setFlash('Você já confirmou presença neste evento', 'error');
				$this->redirect($this->referer());
			}
			
			$this->Presenca->id = $userconfir;

			$this->Presenca->set( array( 
				'confirmado' => 1, 
				'usuario_id' => $this->Session->read('Usuario.id'), 
				'evento_id' => $id, 
				'acompanhante' => $this->request->data['Presenca']['acompanhante']
				));

			if( $this->Presenca->save() ) {
				$this->Session->setFlash('Sua presença foi confirmada com sucesso.', 'success');
				$this->redirect ( array('controller' => 'Eventos', 'action' => 'lista', $id ));
			} else {
				$this->Session->setFlash('Falha ao confirmar presença!', 'error');
			}
		}
		
	}


	public function nao($id){
			if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		$this->loadModel('Presenca');
		$this->Presenca->set(array(
			'id' => $id,
			'confirmado' => 2,
			'acompanhante' => NULL
		));

		if( $this->Presenca->save() ) {

			$this->Session->setFlash('Presenca cancelada com sucesso.', 'success');
			$this->redirect(array( 'controller' 	=> 'Eventos', 'action' => 'index'));
		} else {
			$this->Session->setFlash('Falha ao cancelar presenca!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		//array( 'controller' => 'Tickets', 'action' => 'index' )
	}

	public function cancelar($id){
			if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		$this->loadModel('Presenca');
		$this->Presenca->set(array(
			'id' => $id,
			'confirmado' => 0,
			'acompanhante' => NULL
		));

		if( $this->Presenca->save() ) {

			$this->Session->setFlash('Presenca cancelada com sucesso.', 'success');
			$this->redirect(array( 'controller' 	=> 'Eventos', 'action' => 'index'));
		} else {
			$this->Session->setFlash('Falha ao cancelar presenca!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		//array( 'controller' => 'Tickets', 'action' => 'index' )
	}



	public function adm($st = 1) {
		$this->set('st',$st);

		if($st == 1){
		$eventos = $this->Evento->find('all', array( 'conditions' => array( 'Evento.ativo' => 1 )));
		$this->set('evt',$eventos);
		}else{
			$eventos = $this->Evento->find('all', array( 'conditions' => array( 'Evento.ativo' => 0 )));
			$this->set('evt',$eventos);
		}
	}
	
	public function inserir() {
    if( $this->request->is('post') && !empty($this->request->data)) {

      	$this->Evento->create();
    	if($this->request->data['Evento']['img']['name'] != NULL){
    		$ext = substr(strtolower(strrchr($this->request->data['Evento']['img']['name'], '.')), 1); //get the extension
			$arr_ext = array('jpg', 'jpeg', 'gif', 'png', 'bmp'); //set allowed extensions

			if (!in_array($ext, $arr_ext)) {
				$this->Session->setFlash('Tipo de arquivo não suportado!', 'error');
				$this->redirect($this->referer());
			} else if ( $this->request->data['Evento']['img']['size'] > 10000000) {				
				$this->Session->setFlash('Arquivo maior que 10MB', 'error');
				$this->redirect($this->referer());
			}
			$seg = time();
      		$this->Evento->set(array('imagem' => 'upload/eventos/'.$seg.$this->request->data['Evento']['img']['name']));
      		move_uploaded_file($this->data['Evento']['img']['tmp_name'], WWW_ROOT . DS . 'upload' . DS . 'eventos' . DS .$seg. $this->request->data['Evento']['img']['name'] );
		}

		$this->Evento->save($this->request->data);
		$this->redirect(array( 'controller' => 'Eventos', 'action' => 'adm'));
      }
  }
	

	public function convidar($id){
	$this->loadModel('Usuario');
	$this->loadModel('Departamento');
	$this->loadModel('Notificacoes');


		$this->set('evt',$this->Evento->find('first',array( 'conditions' => array( 'Evento.id' => $id ))));


		$usuarios = $this->Usuario->find('all', array( 'conditions' => array( 'Usuario.ativo' => 1 ), 'recursive' => 1));
		// construtor de query / parametros
		$this->set('users',$usuarios);

		if( $this->request->is('post')) {
			$this->loadModel('Presenca');
			foreach ($this->request->data['Presenca'] as $key => $valor) {
				$this->Presenca->create();
				$this->Presenca->set(array(
					'usuario_id' => $valor['usuario_id'],
					'evento_id' => $id,
					'confirmado' => 0,
					'acompanhante' => ''
				)); 
				$this->Presenca->save();
			}

			


			$Email = new CakeEmail();
			$Email->config('default');
			$Email->to('ti2@miraimotors.com.br');
			$Email->emailFormat('html');
			$Email->subject('[ Recanto da Saúde ] Contato recebido!');

			$mensagem = 'Chegou parca?';

			if( $Email->send($mensagem) ){
				$this->Session->setFlash('Email enviado com sucesso!', 'success');
			}
			else{
				$this->Session->setFlash('Falha ao enviar email, favor tentar novamente!', 'error');
			}

			$this->redirect(array( 'controller' => 'Eventos', 'action' => 'lista', $id));
		}



	}

	public function editar($id) {
		$this->Evento->id = $id;

		if ($this->request->is('get')) {
			$this->request->data = $this->Evento->read();
		} else {
			if ( $this->Evento->save($this->request->data)) {			
				$this->Session->setFlash('Evento editado com sucesso.','success');
                $this->redirect(array( 'controller' => 'Eventos', 'action' => 'adm'));             
            }
		}
	}


	public function ver($id) {
		$this->loadModel('Comentario');
		$this->loadModel('Post');
		$this->loadModel('Usuario');

		
		if ($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){
				$admin = 1;
				$this->set('canAdmin',$admin);

			}else {
				$user = 0;
				$this->set('canAdmin',$user);
			}
	

		$this->set('eventos',$this->Evento->find('first',array( 'conditions' => array( 'Evento.id' => $id ), 'recursive' => 1)));

		$this->set('posts',$this->Post->find('all', array( 'conditions' => array( 'Post.evento_id' => $id ), 'recursive' => 2, 'order' => array('Post.created' => 'desc') )));

		$this->set('numberp',$this->Post->find('count',array( 'conditions' => array('Post.evento_id' => $id ), 'recursive' => 1)));

		$this->set('numberc',$this->Comentario->find('count',array( 'conditions' => array( ), 'recursive' => 1)));

	}

	public function excluir($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}

		$this->Evento->set(array(
			'id' => $id,
			'ativo' => 0
		));

		if( $this->Evento->save() ) {

			$this->Session->setFlash('Evento desativado com sucesso.', 'success');
			$this->redirect(array( 'controller' 	=> 'Eventos', 'action' => 'adm'));
		} else {
			$this->Session->setFlash('Falha ao desativar Evento!', 'error');
		}
		// Redirecionar para ação de onde veio
		// pois não existe View para essa função ( e nem deve existir )
		//array( 'controller' => 'Tickets', 'action' => 'index' )
	}

	
}
