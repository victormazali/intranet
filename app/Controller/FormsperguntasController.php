<?php

class FormPerguntasController extends AppController {

	public function inserir() {
		if( $this->request->is('post')) {
		
			if( $this->Formulario->save($this->request->data) ) {
				$this->Session->setFlash('Formulario criado com sucesso.', 'error');	
				$this->redirect ( array('controller' => 'Formularios', 'action' => 'adm') );
			} else {
				$this->Session->setFlash('Falha ao salvar usuario!', 'error');
			}

		}
	}

}
?>
