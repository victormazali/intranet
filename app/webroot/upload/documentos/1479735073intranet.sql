-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.162
-- Generation Time: 11-Nov-2016 às 11:49
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.26-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qualquernome`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `descricao` varchar(120) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `descricao`, `ativo`, `created`, `updated`) VALUES
(1, 'Críticas', 1, NULL, NULL),
(2, 'Elogios', 1, NULL, NULL),
(3, 'Reclamações', 1, NULL, NULL),
(4, 'Sugestões', 1, NULL, NULL),
(5, 'Solicitações', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `descricao` varchar(120) NOT NULL,
  `can_reply` tinyint(1) NOT NULL DEFAULT '0',
  `can_create_user` tinyint(1) NOT NULL DEFAULT '0',
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `departamentos`
--

INSERT INTO `departamentos` (`id`, `descricao`, `can_reply`, `can_create_user`, `ativo`, `created`) VALUES
(1, 'Recursos Humanos', 1, 1, 1, '2016-05-01'),
(2, 'Administração', 0, 0, 1, '2016-05-01'),
(3, 'Pos Vendas', 0, 0, 1, '2016-05-01'),
(4, 'Vendas', 0, 0, 1, '2016-05-01'),
(5, 'Pecas', 0, 0, 1, '2016-05-01'),
(9, 'AdminTI', 1, 1, 0, '2016-07-05'),
(15, 'Seminovos', 0, 0, 1, '2016-10-05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `titulo` varchar(120) NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `forms`
--

INSERT INTO `forms` (`id`, `usuario_id`, `created`, `updated`, `titulo`, `ativo`) VALUES
(3, 7, NULL, NULL, 'Prospecção', 1),
(4, 7, NULL, NULL, 'Lideres', 1),
(8, 7, '2016-07-01', '2016-07-01', 'testetitulo', 1),
(9, 7, '2016-07-01', '2016-07-01', 'teste1', 1),
(10, 7, '2016-07-01', '2016-07-01', 'teste4', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `formsperguntas`
--

CREATE TABLE `formsperguntas` (
  `id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `pergunta1` varchar(255) NOT NULL,
  `pergunta2` varchar(255) DEFAULT NULL,
  `pergunta3` varchar(255) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `formsrespostas`
--

CREATE TABLE `formsrespostas` (
  `id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL,
  `formsperguntas_id` int(11) NOT NULL,
  `resposta1` text NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas`
--

CREATE TABLE `respostas` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `conteudo` text NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `respostas`
--

INSERT INTO `respostas` (`id`, `ticket_id`, `usuario_id`, `conteudo`, `created`) VALUES
(1, 3, 7, 'resposta?', '2016-05-09 14:13:35'),
(2, 3, 7, 'reesposta2', '2016-05-09 14:38:33'),
(3, 4, 7, 'resposta?\r\n', '2016-05-09 19:50:48'),
(4, 3, 7, 'fsdfssfdfgsgds', '2016-05-19 20:25:36'),
(5, 3, 7, 'fsdadfsafds', '2016-05-23 15:10:09'),
(6, 3, 7, 'teste 212 112', '2016-05-23 15:12:18'),
(7, 5, 7, 'resposta1 24', '2016-05-23 15:15:07'),
(8, 6, 7, 'Resposta 1', '2016-05-27 13:18:44'),
(9, 6, 7, 'Resposta 2', '2016-05-27 13:23:44'),
(10, 6, 7, 'Resposta 3', '2016-05-27 13:24:09'),
(11, 8, 7, 'restefsdfsd', '2016-05-27 13:50:36'),
(12, 7, 7, 'teste', '2016-05-27 14:09:43'),
(13, 8, 7, 'teste', '2016-05-27 14:10:28'),
(14, 5, 7, 'refafsdfds', '2016-05-30 20:33:48'),
(15, 5, 7, 'fsdsfd', '2016-05-30 20:33:52'),
(16, 5, 7, 'Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.', '2016-06-02 17:45:41'),
(17, 5, 7, 'Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.', '2016-06-02 17:45:42'),
(18, 9, 7, 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', '2016-06-02 17:50:04'),
(19, 9, 9, 'Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do "de Finibus Bonorum et Malorum" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, "Lorem Ipsum dolor sit amet..." vem de uma linha na seção 1.10.32.', '2016-06-03 14:56:35'),
(20, 10, 8, 'tefdasfdsfdsfdsfds', '2016-06-03 19:45:11'),
(21, 10, 8, 'fdsfdsfdsdffsdfds', '2016-06-03 19:45:27'),
(22, 10, 7, 'teste', '2016-06-04 17:06:53'),
(23, 9, 7, 'O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.', '2016-06-30 15:12:55'),
(24, 14, 7, 'testeteetette', '2016-07-01 13:56:31'),
(25, 9, 7, 'teste', '2016-07-01 15:45:45'),
(26, 5, 7, 'fdsfdsffds', '2016-07-01 19:16:49'),
(27, 5, 7, 'fdsfsdfs', '2016-07-01 19:22:51'),
(28, 5, 7, 'fdsfsdfsd', '2016-07-01 19:24:04'),
(29, 5, 7, 'fdssfdsfddfs', '2016-07-01 19:32:46'),
(30, 5, 7, 'fdsdfsfsdfdssfd', '2016-07-01 19:34:40'),
(31, 5, 7, 'fsdfdsfds', '2016-07-01 19:37:19'),
(32, 5, 7, 'fdsfsdfsd', '2016-07-01 19:40:47'),
(33, 5, 7, 'gfgfdgfd', '2016-07-01 19:42:53'),
(34, 5, 7, 'fsdfdsfsfs', '2016-07-01 19:47:06'),
(35, 6, 7, 'fdsfdssdf', '2016-07-01 19:48:51'),
(36, 6, 7, 'fdsfdfdfd', '2016-07-01 19:50:42'),
(37, 6, 7, 'fdsfdsfd', '2016-07-01 19:50:53'),
(38, 20, 10, 'fsdfdfsd', '2016-07-01 20:43:52'),
(39, 20, 7, 'fsddsfdsf', '2016-07-01 20:57:01'),
(40, 14, 7, 'dfssdffds', '2016-07-01 20:57:05'),
(41, 20, 10, 'fdsfds', '2016-07-01 21:21:50'),
(42, 20, 10, 'fdsfds', '2016-07-01 21:21:50'),
(43, 20, 10, 'gffgdsfg', '2016-07-01 21:24:49'),
(44, 20, 10, 'gfggf', '2016-07-01 21:27:10'),
(45, 6, 10, 'fdsfds', '2016-07-04 13:18:52'),
(46, 6, 10, 'fsafsdaa', '2016-07-05 14:44:56'),
(47, 6, 10, 'sfdsdfsfd', '2016-07-05 15:40:30'),
(48, 6, 7, 'fdsfdsf', '2016-07-05 15:42:02'),
(49, 6, 10, 'gffdgfdgfd', '2016-07-05 20:40:14'),
(50, 9, 10, 'fdsfsd', '2016-07-06 22:36:13'),
(51, 9, 7, 'fdsfdsfds', '2016-07-06 22:36:55'),
(52, 10, 10, 'fdsfdsdfs', '2016-07-07 13:23:14'),
(53, 9, 10, 'ffdsfsdfsd', '2016-09-30 05:04:34'),
(54, 30, 10, 'fdsfdsfds', '2016-09-30 05:04:39'),
(55, 30, 10, 'fsdfdsfsd\r\n', '2016-09-30 05:04:54'),
(56, 30, 10, 'sfdfsdsfd', '2016-09-30 05:05:11'),
(57, 10, 10, 'testeee', '2016-10-01 22:37:47'),
(58, 31, 10, 'respondidoadmin', '2016-10-01 22:41:37'),
(59, 32, 10, 'admin respondeu', '2016-10-01 23:21:28'),
(60, 32, 12, 'eu respondi', '2016-10-01 23:22:04'),
(61, 38, 19, 'respndidssdfds', '2016-10-05 20:33:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `descricao` varchar(20) NOT NULL,
  `ativo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `status`
--

INSERT INTO `status` (`id`, `descricao`, `ativo`) VALUES
(0, 'Aberto', 1),
(1, 'Em andamento', 1),
(2, 'Aguardando usuario', 0),
(3, 'Encerrado', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `conteudo` text NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  `anonimo` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `departamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `cpf`, `senha`, `created`, `ativo`, `departamento_id`) VALUES
(10, 'admin Mirai', '999.999.999-99', 'fca8777652e0b9c8708aa7bb3da2691e8429b8e1ecbc3d4745d50524346c376a', '2016-07-01', 0, 1),
(11, 'user Mirai', '111.111.111-11', 'fca8777652e0b9c8708aa7bb3da2691e8429b8e1ecbc3d4745d50524346c376a', '2016-07-01', 1, 3),
(18, 'Rezim Junior', '408.228.448-69', 'fca8777652e0b9c8708aa7bb3da2691e8429b8e1ecbc3d4745d50524346c376a', '2016-10-05', 0, 2),
(19, 'Angelica Farias', '383.729.948-11', '904569c6dff3ab2e5518af68d7c0b8b26962ce08d366b4c2591389f61d98b1b3', '2016-10-05', 1, 1),
(20, 'Victor Henrique', '424.069.388-83', 'fca8777652e0b9c8708aa7bb3da2691e8429b8e1ecbc3d4745d50524346c376a', '2016-10-05', 1, 2),
(21, 'Acauãn Guedes Pinto', '443.670.708-32', '128b939d7afcf2239f63235649b7cd77d1d9d10e41836f937ab8328e29fa32a5', '2016-10-31', 1, 3),
(22, 'Adriana Marques', '283.694.198-79', 'c94ba23bc4770ba6c4ba24448d76223bdbc003f1b49e432ffd7e34aa9941890d', '2016-10-31', 1, 4),
(23, 'Alessandra da Silva Tavares', '435.671.088-67', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 2),
(24, 'Amanda Alves Corrêa', '201.546.955-18', '3a1b5d50688afda74ce93216cefe834a35316619d58d75ea79a9e6eb297c2ab6', '2016-10-31', 1, 4),
(25, 'Antônia Souto da Silveira Maceno', '257.542.168-31', 'ea344605f29ab3c136c8351dfbbc5756b3b608365aad5b134e891db947620e74', '2016-10-31', 1, 4),
(26, 'Antônio Carlos Mazzo Rodrigues', '287.737.758-00', 'e58d40c0021e3ea1e5e2815bbe6dcb4685e56525c30404b7f713348ac3993c7e', '2016-10-31', 1, 3),
(27, 'Bianca Caroline Netto', '423.663.238-16', '04bd07a6eebf57f2562ca57d418b597b450e61cd8cc898dab0ad6fea422288a6', '2016-10-31', 1, 3),
(28, 'Bruno de Paula Telles', '416.835.848-80', '542d45016b2ad555f8c0eb78dd383712daaa861ce5cf4bb8a56cbbd49ea3827f', '2016-10-31', 1, 3),
(29, 'Bruno Sérgio Ramos', '389.549.038-51', '51b8deee1579dcd8c38c72b1c2cb58f680dd21fb983717238a9f9638e0766cfb', '2016-10-31', 1, 3),
(30, 'Camila da Silva', '466.165.578', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 15),
(31, 'Carla Carolina dos Santos Aguiar', '444.071.458-70', 'e002a244543607ae04b635ad10a08a94201d21f479158e381a3db3cdbe7eaa6d', '2016-10-31', 1, 3),
(32, 'Carlos Augusto Jandote', '223.649.218-99', 'c1da9883559c38c34be5e354d586126f49a59a99abfac1e83beeb14ce3197906', '2016-10-31', 1, 2),
(33, 'Cassiano Alex Berbel', '213.639.098-86', '3cb8db7039dd77e4ecdddcbfab2cd461c7ce02641fb5c5d8dcd04de43cb7d9d9', '2016-10-31', 1, 3),
(34, 'César Augusto Pedroso da Silva', '385.542.568-08', 'a767c8739fb6ef1e7295bc1d69e77b9e01f727bbad888e05cd441af1a37829ca', '2016-10-31', 1, 3),
(35, 'Charbeel Lunardi Mendez', '303.865.288-85', 'e1f8d653cbd765b05d6f51521eebf1f1d957569c4140216762a3d80acb86c760', '2016-10-31', 1, 3),
(36, 'Daniel Colombo Souza', '335.272.848-85', 'dea3bf910442b3c2a340587e05c85513c1e6c4dce50ce3fdfde51a4f865f0044', '2016-10-31', 1, 3),
(37, 'Darlene Pereira Vargas dos Santos', '352.846.888-21', '808377030ff978c9048e3d287c850b24c171c491c435783235037f7e14075ddd', '2016-10-31', 1, 2),
(38, 'Douglas Ferrari Gigliotti', '301.349.258-90', '3c3c6cf1b1dd331a2c74b7c0c5931259a40bdd3d2bcd757ba9bcd0e744ed2c5d', '2016-10-31', 1, 3),
(39, 'Edson Roberto Chiqueti', '120.050.628-63', '38426d1dbf459640eed83e2a6712dba98e379310c6f58766eace32b9006954bf', '2016-10-31', 1, 3),
(40, 'Elaine Cristina Bortoletto Freire', '301.349.258-90', 'a347d8860edef199c153e75eed4a8de2f2cda120850f6d69ab42b97bedba20bc', '2016-10-31', 1, 15),
(41, 'Eliezer Ferreira  Rufino', '361.343.588-86', '1bb92370f8d0955217a0bec77063b5aaed571407ad344c9534482bfb1b6c0595', '2016-10-31', 1, 3),
(42, 'Everton José de Mattos Bossoni', '170.381.348-09', 'f7fd71ee72e010012af53276ed06c6492d1dccf0367b746ec03fc004db383afa', '2016-10-31', 1, 4),
(43, 'Fabiano Diogo do Nascimento Peran', '327.174.118-28', 'baaa144f0ed436af78a3aa5deac036169cee05f195cc80fadfa861bca751cfbe', '2016-10-31', 1, 3),
(44, 'Fabiano Nunes da Silva', '360.540.408-12', 'f81e269c6ed7c1f90c078298ea46c4baa4f8eba61e255841e918a0f2eb290b7c', '2016-10-31', 1, 4),
(45, 'Fabio Aparecido de Moraes Rufino', '393.507.538-39', '04c579ac1d362bc054d21f08aa9d11b205408b1fc47672614a067f2d902c78d4', '2016-10-31', 1, 2),
(46, 'Fabio Luiz Montenotti', '278.127.418-69', 'ee1e2362f55b26d51d2f17f4296e58c4f80398d27d2a6647fc8d35599c4057ab', '2016-10-31', 1, 5),
(47, 'Fernando Francisco Dias Romão', '304.586.168-77', '73e27b7ab60818646adeec3164308ad027472ce474b8fc31c6b453735b47c4a4', '2016-10-31', 1, 4),
(48, 'Guilherme Henrique Quirino da Cruz', '371.971.958-88', '6615ca0c2529cd8e8171085adb120937bdb8d7b85e372f6a6d60480fb7e250ee', '2016-10-31', 1, 15),
(49, 'Guilherme Silva Leati', '387.918.318-06', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 3),
(50, 'Guilherme Souza Granciere', '385.241.978-62', 'c1e1e451eaa97d21999c776e27ef8bfa057ac2d5c29cbbc5e313c03af1a41c76', '2016-10-31', 1, 3),
(51, 'Guilherme Vitorino Serafim da Silva', '460.674.388-36', 'f7f640de45d966cf704516e64d6fb00f3da90ed790321ac4de9ab7333ae63f66', '2016-10-31', 1, 3),
(52, 'Gustavo Coutinho Prado', '373.603.558-69', '7d5721ca72e2d27069a345853cf0534cd2f596050a0c54a1055638765b7c0dcf', '2016-10-31', 1, 3),
(53, 'Helton Gregório Cupertino', '401.106.358-89', 'ac229b469f4a11d736c6ca6c350ea165ca2072bee7fdb690df27b8dfa40cd1e0', '2016-10-31', 1, 15),
(54, 'Iago Henrique Candido Pereira', '425.816.298-19', '2cc700e366a4b1c71b36ff8abc8d0bfe38055fdb634ce750f893e8b13278a2f3', '2016-10-31', 1, 3),
(55, 'Iara Bueno Souza Batista', '373.934.128-96', '1b9391d68019da4cf25740f48c92214c5f2865a38c50fc0ed57133991a4a2c45', '2016-10-31', 1, 4),
(56, 'Jaime Augusto Braga', '350.760.618-16', '779a537e59fc36c0cd7bae049c1a8d7ac60093ea29091bb3c79365615da86d10', '2016-10-31', 1, 5),
(57, 'Jessica dos Santos Espadoto', '418.598.958-08', 'e002a244543607ae04b635ad10a08a94201d21f479158e381a3db3cdbe7eaa6d', '2016-10-31', 1, 3),
(58, 'João Victor Silva Galende', '428.608.438-84', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 3),
(59, 'Josceli Silva', '035.506.528-28', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 4),
(60, 'José Henrique Coutinho Vaccaro', '450.493.988-31', '7d5721ca72e2d27069a345853cf0534cd2f596050a0c54a1055638765b7c0dcf', '2016-10-31', 1, 2),
(61, 'Karina Beatriz da Silva', '367.665.208-84', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 15),
(62, 'Leonardo da Silva Gonçalves de Paula', '378.788.778-40', '42e5a8e0d491e6670d27f6d4773e0ee112c3f741bc3218740e1920576d873294', '2016-10-31', 1, 4),
(63, 'Leticia Pereira Veloso', '413.845.528-04', '808377030ff978c9048e3d287c850b24c171c491c435783235037f7e14075ddd', '2016-10-31', 1, 3),
(64, 'Liliane de Souza Gomes', '336.316.748-21', 'c1e1e451eaa97d21999c776e27ef8bfa057ac2d5c29cbbc5e313c03af1a41c76', '2016-10-31', 1, 4),
(65, 'Lucas Carmichael de Oliveira', '417.871.338-85', '397f0fbff60ab0e9ba6aec21a08b0b4ad165f26ee649738085647939ed1f03de', '2016-10-31', 1, 5),
(66, 'Marcel Davanso Dutra', '021.798.981-19', '9f3134e9b3df46af695df3da237355eccfa6d1330879c3efde5ccfa7fb28b164', '2016-10-31', 1, 15),
(67, 'Marilia Simião de Oliveira Zanca', '217.609.608-88', '8bc474754648b8396d4ecc1908ec9884514b642b91daab84b39531358dfa5079', '2016-10-31', 1, 4),
(68, 'Marina Azevedo Cabelo Salatini', '343.323.378-63', '1ce21b93f38cae4c970d1ec2359eb5ba7219503d8bd11aa4438f13c89a0ef729', '2016-10-31', 1, 3),
(69, 'Moises Gomes Lauzana', '321.966.158-03', '645532870d28c7b40123eac915aac403803953669c4451106fb8b61b7368f5f9', '2016-10-31', 1, 15),
(70, 'Murilo Henrique Coutinho Leão', '470.185.018-73', '7d5721ca72e2d27069a345853cf0534cd2f596050a0c54a1055638765b7c0dcf', '2016-10-31', 1, 3),
(71, 'Natalia Dias Perin', '395.327.528-38', '73e27b7ab60818646adeec3164308ad027472ce474b8fc31c6b453735b47c4a4', '2016-10-31', 1, 3),
(72, 'Nayara Satiko Inamasu', '417.356.038-99', 'b5df84f3a02a3533c1c085d8ca5acc9d178cb243cb75970b800bd8e49a717e4d', '2016-10-31', 1, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formsperguntas`
--
ALTER TABLE `formsperguntas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formsrespostas`
--
ALTER TABLE `formsrespostas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `respostas`
--
ALTER TABLE `respostas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `formsperguntas`
--
ALTER TABLE `formsperguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `formsrespostas`
--
ALTER TABLE `formsrespostas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `respostas`
--
ALTER TABLE `respostas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
