<?php

class Formulario extends AppModel {
	public $recursive = -1;

	public $belongsTo = array(
		'Usuario', 'FormCategoria'
	);

	public $hasMany = array( 'FormPergunta', 'FormUser' );
}

?>