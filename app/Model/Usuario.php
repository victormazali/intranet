<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class Usuario extends AppModel {
	public $recursive = -1;
	public $belongsTo = array(
		'Departamento'
	);

	public function beforeSave($options = array()) { // Criptografia da senha antes de salvar
		if( !empty($this->data[$this->alias]['senha'])) {

			$passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256')); 

            $this->data[$this->alias]['senha'] = $passwordHasher->hash(
                $this->data[$this->alias]['senha']
            );

            return true;
		} else if ( isset($this->data[$this->alias][$this->primaryKey] )) { // Editando
			$usuario = $this->find('first', array( 'conditions' => array( 'id' => $this->data['Usuario']['id'])));
			$this->data[$this->alias]['senha'] = $usuario['Usuario']['senha'];
		}
	}
	public $validate = array(
		'departamento_id' => array(
		array( 'rule' => 'NotEmpty', 'message' => 'Departamento deve ser preenchida.')
		)
	);

}

?>