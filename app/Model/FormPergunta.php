<?php

class FormPergunta extends AppModel {
	public $recursive = -1;
	public $belongsTo = array(
		'Usuario', 'Formulario'
	);

	public $hasMany = array( 'FormResposta' );
}

?>