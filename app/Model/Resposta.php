<?php

class Resposta extends AppModel {
	public $recursive = -1;
	public $belongsTo = array(
		'Usuario', 'Ticket'
	);

	public function afterFind($results,$primary = false) {
		foreach ($results as $key => $r) {
			if( $results[$key]['Resposta']['anonimo'] == 1){
				$results[$key]['Usuario']['nome'] = "Anônimo";
				$results[$key]['Usuario']['imagem'] = "upload/perfil/anonimo.png";
			}	
		}
		return $results;
	}
}

?>