<?php

class Ticket extends AppModel {
	public $recursive = -1;

	public $belongsTo = array(
		'Usuario', 'Status', 'Categoria'
	);

	public $hasMany = array( 'Resposta');
	
	public function afterFind($results,$primary = false) {
		foreach ($results as $key => $r) {
			if( $results[$key]['Ticket']['anonimo'] == 1){
				$results[$key]['Usuario']['nome'] = "Anônimo";
				$results[$key]['Usuario']['imagem'] = "upload/perfil/anonimo.png";
			}	
		}
		return $results;
	}

	public $validate = array(
		'categoria_id' => array(
		array( 'rule' => 'NotEmpty', 'message' => 'Categoria deve ser preenchida.')
		)
	);
}

/*
public function afterFind(array $results, boolean $primary = false){

	$data = $this->find('first',array( 'conditions' => array( 'anonimo' =>  1)));
	if($data == NULL){
		$results = 'usuario_id';
	}else
	$results = "Anônimo";
}

---------------------------------------------------------------------


	public function afterFind(array $results, boolean $primary = false){

		$results = array(
		    0 => array(
		        'usuario_id' => array( 'conditions' => array( 'anonimo' => 1 )
		            'usuario_id' => "Anônimo";
		        ),
		    ),
		);

	}

----------------------------------------------------------------------

	$data = $this->find('first',array( 'conditions' => array( 'anonimo' =>  1)));
	if($data == NULL){
		return 'usuario_id';
	}else
	return 'usuario_id' == "Anônimo";
*/


?>

