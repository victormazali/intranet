<?php

class Evento extends AppModel {
	public $recursive = -1;

	public $hassMany = array(
		'Presenca'
	);

	public function beforeSave($options = array()) {
    	if( $this->data['Evento']['data'] && $this->data['Evento']['hora'] ) {
    		$data = explode('/', $this->data['Evento']['data']);
    		$dataFormatada = $data[2].'-'.$data[1].'-'.$data[0];
    		$this->data['Evento']['data'] = $dataFormatada.' '.$this->data['Evento']['hora'].':00';
    	}
	}
}
