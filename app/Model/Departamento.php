<?php

class Departamento extends Model {
	public $recursive = -1;


	public $validate  = array(
		'descricao' => array(
			 'rule' => array('maxLength', '120'),
            'message' => 'Mínimo de 120 caractéres!'
        )
	);

	public function hasPerm($id,$perm) {
		$data = $this->find('first',array( 'conditions' => array( 'id' => $id )));

		if( isset($data['Departamento']["$perm"]))
			return $data['Departamento']["$perm"];
		else
			return 0;
	}
}

?>