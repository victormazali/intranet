<?php echo $this->Form->create('Evento', array( 'type' => 'file', 'inputDefaults' => array( 'label' => false ))) ?>
<div class="ls-box-filter ls-form ls-form-inline ls-float-left">
<b class="ls-label-text">Nome do evento:</b>
<?php echo $this->Form->input('descricao') ?>
<br>

<b class="ls-label-text">Data do Evento:</b>
<br>
<form action="" class="ls-form ls-form-horizontal" data-ls-module="form">
  <label class="ls-label col-md-3">
    <?php echo $this->Form->input('data', array('type' => 'text', 'class' => 'datepicker', 'placeholder' => 'dd:mm:aaa')) ?>
  </label>
</form>
<br>
<br>
<br>
<b class="ls-label-text">Hora do Evento:</b>
<?php echo $this->Form->input('hora', array('type' => 'text','data-mask' => "99:99")) ?>
<br>
<b class="ls-label-text">Local do evento:</b>
<?php echo $this->Form->input('local') ?>
<br>
<b class="ls-label-text">Imagem do evento:</b>
<br>
<br>
<?php echo $this->Form->input('img', array( 'type' => 'file','style' => 'width:100%;')); ?>
<hr>
<button class="ls-btn"">Salvar</button>
<?php echo $this->Form->end(); ?>