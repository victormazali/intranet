<header class="ls-info-header">
      <h2 class="ls-title-3"><?php echo $evento['Evento']['descricao']?> => Lista de Presença</h2>
</header>

<br>
	<div class="ls-box">
<?php if ($jaConfirmou['Presenca']['confirmado'] == 1) { ?>
<?php echo $this->Form->postLink(
                  '   <button class="ls-btn-danger ls-ico-close">Cancelar Presença</button>',
                  array('controller' => 'Eventos', 'action' => 'cancelar', $jaConfirmou['Presenca']['id']),
                  array('confirm' => 'Deseja cancelar sua presenca?', 'class' => 'btn btn-danger', 'escape' => false)); ?>	
<?php } else { ?>	
	
<?php echo $this->Form->create('Presenca', array( 'inputDefaults' => array( 'label' => false ))) ?>


	
		<div id="box-toggle">
			<div class="tgl">
			<br>
				<div class="ls-box">
				 
					<div class="ls-alert-info"><strong>Atenção: </strong>Somente um acompanhante por colaborador. Para adicionar mais acompanhantes, por favor, entre em contato com nosso RH</div>
				  <b class="ls-label-text">Nome do Acompanhante:</b>
					<?php echo $this->Form->input('acompanhante', array('placeholder' => 'Deixe em branco para nao incluir acompanhante','style'=>'width:450px; ')) ?>

				</div>
			</div>
		</div>
<br>
<br>
<button class="ls-btn ls-ico-checkmark">Confirmar Presença</button>
<?php echo $this->Form->end(); ?>
<?php } ?>
<hr>

<div class="ls-box col-md-4 ls-xs-space ls-color-success">
  <h5 class="ls-title-3 ls-color-success ls-ico-checkmark"> (<?php echo $number + $acp ?>) Confirmados</h5>
  <p>(<?php echo $number ?>) Colaboradores</p>
  <p>(<?php echo $acp ?>) Acompanhantes</p>
  <p class="ls-color-info">(<?php echo $conv ?>) Convidados</p>
</div>



</div>

<div class="container col-md-12 col-md-offset">
  <hr>
</div>

<table class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th style="width: 100px;">Confirmado</th>
			<th>Colaborador</th>
			<th>Acompanhante</th>
		
		</tr>
	</thead>
	<tbody>	
		<?php foreach( $presenca as $d ) { ?>
				
				<?php if( $d['Presenca']['confirmado'] == 1){?>
					<td class="ls-ico-checkbox-checked ls-color-success">Sim</td>
					<?php }else{?>
						<td class="ls-ico-checkbox-unchecked ls-color-info">Nao</td>
					<?php } ?>
				<td><?php echo $d['Usuario']['nome']?></td>
				<td><?php echo $d['Presenca']['acompanhante'] ?></td>
				
			</tr>
		<?php } ?>
	
	</tbody>
</table>
<hr>



<script type="text/javascript">
$(document).ready( function () {
	$("#dpstatus").change(function() {
		var parametro = $('option:selected', "#dpstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Eventos/index/'+parametro;
	});
});

jQuery.fn.toggleText = function(a,b) {

return   this.html(this.html().replace(new RegExp("("+a+"|"+b+")"),function(x){return(x==a)?b:a;}));

}

 

$(document).ready(function(){

$('.tgl').before('<span> <button class="ls-btn ls-btn-xs ls-ico-plus"></button> Adicionar acompanhante  </span>');

$('.tgl').css('display', 'none')

$('span', '#box-toggle').click(function() {

$(this).next().slideToggle('slow')

.siblings('.tgl:visible').slideToggle('fast');

// aqui começa o funcionamento do plugin

$(this).toggleText('Revelar','Esconder')

.siblings('span').next('.tgl:visible').prev()

.toggleText('Revelar','Esconder')

});

})

</script>