<header class="ls-info-header">
      <h2 class="ls-title-3"><?php echo $eventos['Evento']['descricao']?> => Time Line => (<?php echo $numberp?>) Posts</h2>
</header>
<?php if($canAdmin == 1) {?>
<?php echo $this->Form->create('Post', array( 'type' => 'file','inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'postar'))) ?>
<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 1px;
                  
                          ">

<?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Comente algo...','style'=>'width:100%; margin: 3px;')) ?>
<br>
<p class="ls-float-right ls-float-none-xs ls-small-info">
      <button class="ls-btn-primary">Postar</button>
    </p>
 <span class="ls-btn-primary">
 <?php echo $this->Form->input('img', array( 'type' => 'file')); ?>
  </span> 
</div>
<?php echo $this->Form->hidden('evento_id', array( 'value' => $eventos['Evento']['id'] )) ?>
<?php echo $this->Form->end(); ?>
<?php }?>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<hr>
<br>
<br>


<?php foreach( $posts as $p)  { ?>

<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 5px;
                          ">
  <header class="ls-info-header">
  <img src="<?php echo $this->webroot.$p['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" />
    <h2 class="ls-title-3"><?php echo $p['Usuario']['nome'] ?></h2>
    <p class="ls-float-right ls-float-none-xs ls-small-info">
      Postado em <strong><?php echo date('d/m/Y', strtotime($p['Post']['created'])) ?> as <?php echo date('H:i:s', strtotime($p['Post']['created'])) ?></strong>
    </p>
  </header>

  <p><?php echo nl2br(h($p['Post']['conteudo'])) ?></p>
  <?php if($p['Post']['imagem'] != NULL ) { ?>
  <img src="<?php echo $this->webroot.$p['Post']['imagem'] ?>" height="300" />
  <?php } ?>
  <br>
  <br>
<br>
  <footer class="ls-info-header">
<?php echo $this->Form->create('Comentario', array( 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'comentar'))) ?>
  	<?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Escreva um comentario','style'=>'width:100%;')) ?>
  <?php echo $this->Form->hidden('post_id', array( 'value' => $p['Post']['id'] )) ?>
  <p class="ls-float-right ls-float-none-xs ls-small-info">
 <button class="ls-btn-primary">Comentar</button>
 </p>
<?php echo $this->Form->end(); ?>

  </footer>
  	<div id="box-toggle">
      <span class="openComentario">
        <p class="ls-cursor-pointer ls-txt-center ls-color-info">
          Ver Comentarios (<?php echo count($p['Comentario']) ?>)
        </p>
      </span>
			<div class="tgl comentario" style="display:none;">
          <br>
				 			<?php foreach( $p['Comentario'] as $c)  { ?>
							<div class="ls-box ls-xs-space ls-txt-left ">
              <p class="ls-float-right ls-float-none-xs ls-small-info">
              Postado em <strong><?php echo date('d/m/Y', strtotime($c['created'])) ?> as <?php echo date('H:i:s', strtotime($c['created'])) ?></strong>
              </p>
						  <h5 class="ls-title-5"> <img src="<?php echo $this->webroot.$c['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" /> <?php echo $c['Usuario']['nome'] ?></h5>

						  <p> <?php echo $c['conteudo'] ?> </p>
							</div>
							<?php } ?>
					
			</div>
	</div>
</div>
<div class="container col-md-12 col-md-offset">
  <hr>
</div>

<?php } ?>



<?php echo $this->Form->end(); ?>

<script type="text/javascript">

$(document).ready(function(){
  $(".openComentario").click(function() {
    $(this).next().slideToggle()();
  });
})

</script>