<a href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'inserir')) ?>">
	<button class="ls-btn ls-ico-plus">Criar Novo Evento</button>
</a>

<!-- BOX DE FILTRO-->
<div class="ls-box-filter">
  <form action="" class="ls-form ls-form-inline ls-float-left">
    <label class="ls-label col-md-8 col-sm-8">
      <b class="ls-label-text">Status</b>
      <div class="ls-custom-select">
        <select id="evtstatus" class="ls-select">
        
          <option filtro="1" <?php if( $st == 1) echo 'selected="selected"' ?> >Ativos</option>
    
          <option filtro="0" <?php if( $st == 0) echo 'selected="selected"' ?>  >Desativados</option>
        
        </select>
      </div>
    </label>
  </form>
</div>
<!-- BOX DE FILTRO TERMINA-->

<!-- BOX DE FILTRO DE EXIBICAO -->



<table class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th>Descrição</th>
			<th>Data</th>
			<th>Local</th>
			<th>Criado Em</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $evt as $d ) { ?>
			<tr>
				<td><?php echo $d['Evento']['descricao'] ?></td>
				<td><?php echo date('d/m/Y H:i',strtotime($d['Evento']['data'])); ?></td>
				<td><?php echo $d['Evento']['local'] ?></td>
				<td><?php echo date('d/m/Y',strtotime($d['Evento']['created'])); ?></td>
				<td>
					<a href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'convidar', $d['Evento']['id'])) ?>">
						<button class="ls-btn ls-ico-user-add"></button>
					</a>
					<a href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'editar', $d['Evento']['id'])) ?>">
						<button class="ls-btn ls-ico-pencil"></button>
					</a>
					<?php echo $this->Form->postLink(
	                '<button class="ls-btn-danger ls-ico-remove"></button>',
	                array('action' => 'excluir', $d['Evento']['id']),
	                array('confirm' => 'Deseja excluir o departamento?', 'class' => 'btn btn-danger', 'escape' => false)); ?>	
				</td>
			</tr>
		<?php } ?>
	
	</tbody>
</table>
<hr>
<script type="text/javascript">
$(document).ready( function () {
	$("#evtstatus").change(function() {
		var parametro = $('option:selected', "#evtstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Eventos/adm/'+parametro;
	});
});
</script>