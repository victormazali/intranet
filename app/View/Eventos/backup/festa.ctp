<h1>Lista de Presença </h1>
<br>
<br>

<table class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th>Colaborador</th>
			<th>Acompanhante</th>
		
		</tr>
	</thead>
	<tbody>	
		<?php foreach( $evt as $d ) { ?>
				<td><?php echo $d['Usuario']['nome'] ?></td>
				<td><?php echo $d['Usuario']['acompanhante'] ?></td>
				
			</tr>
		<?php } ?>
	
	</tbody>
</table>
<hr>

<?php echo $this->Form->checkbox('festa', array('hiddenField' => 0)); ?> Confirmar presença
<br>

<b class="ls-label-text">Acompanhante:</b>
<?php echo $this->Form->input('acompanhante') ?>
<br>


<script type="text/javascript">
$(document).ready( function () {
	$("#dpstatus").change(function() {
		var parametro = $('option:selected', "#dpstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Eventos/index/'+parametro;
	});
});
</script>