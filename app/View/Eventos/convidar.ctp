<header class="ls-info-header">
      <h2 class="ls-title-3"><?php echo $evt['Evento']['descricao']?> => Lista de Convidados</h2>
</header>


<?php echo $this->Form->create('Evento', array( 'inputDefaults' => array( 'label' => false, 'div' => false))) ?>
<table id="convidadosTable" cellspacing="0" class="display select ls-table ls-table-striped">
	<thead>
		<tr>
			<th style="width: 13px;"><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
			<th>Nome</th>
			<th>CPF</th>
			<th>Departamento</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $users as $key => $u ) { ?>
			<tr>
				<td>
               <input type="checkbox" value="<?php echo $u['Usuario']['id'] ?>" name="data[Presenca][<?php echo $key ?>][usuario_id]" class="marcar">
            </td>
				<td class="ls-ico-user"> <?php echo $u['Usuario']['nome'] ?></td>
				<td><?php echo $u['Usuario']['cpf'] ?></td>
				<td><?php echo $u['Departamento']['descricao'] ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<hr>
<button class="ls-btn ls-ico-checkmark">Confirmar Convidados</button>
<?php echo $this->Form->end(); ?>

<hr>
<script type="text/javascript">

$(document).ready(function (){
   var table = $('#convidadosTable').DataTable({
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
            console.log(data);
             return data;
         }
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#example tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element 
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         } 
      });
   });

});

</script>