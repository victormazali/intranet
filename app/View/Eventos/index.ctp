<?php foreach( $evt as $r)  { ?>

<div class="ls-box ls-board-box" style="
                          background: #CFCFCF;
                        
                          ">
    <a href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'ver', $r['Evento']['id'])) ?>">
    <header class="ls-info-header" style="
                          background: #363636;">
      <h2 class="ls-title-3" style="color : #F8F8FF;"><?php echo $r['Evento']['descricao']?></h2>
      <p class="ls-float-right ls-float-none-xs ls-small-info" style="color : #F8F8FF;" >
      Ultima atualização em <strong><?php echo $r['Evento']['updated']?></strong></p>
    </header>
    </a>

    <!-- -->
    <div id="sending-stats" class="row">
    <div class="col-sm-6 col-md-3">
      <div class="ls-box">
       <?php if($r['Evento']['imagem'] != NULL) {?>
        <img src="<?php echo $this->webroot.$r['Evento']['imagem'] ?>" alt="" width="250" height="300"/>
      <?php } else {?>
      <img src="<?php echo $this->webroot ?>img/default.png" width="250" height="300" />
      <?php }?>
      </div>
    </div>
    <!-- -->

     <!-- -->
    <div class="col-sm-6 col-md-6">
      <div class="ls-box">

        <div class="ls-box-head">
          <h2><span class="ls-ico-info">Informações</span></h2>
        </div>
        <br>
        <div class="ls-box-body">

            <a> <label class="ls-label"> Local : <?php echo $r['Evento']['local'] ?></a>

            <a class="ls-label ls-color-info">
                      (<?php echo $r['Evento']['posts']?>)Posts</a>
                    <a href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'ver', $r['Evento']['id'])) ?>">
                      <button class="ls-btn ls-ico-panel-hospedagem">Timeline</button>
                    </a>
         
        </div>
        <br>
        <br>
        <br>
        <div class="ls-box-footer">
                    <a class="ls-label ls-color-success">
                    
                      (<?php echo $r['Evento']['confirmados'] + $r['Evento']['acompanhantes'] ?>) Confirmados</a>
            
                   <a class="ls-label ls-color-info">
                    
                      (<?php echo $r['Evento']['convidados']?>) Convidados</a>

                   <a class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'lista', $r['Evento']['id'])) ?>';">
                   <button class="ls-btn ls-ico-users">Lista de Presença</button>
                   </a>
               
        </div>

        
      </div>
    </div>
    <!-- -->

    <!-- -->
    <div class="col-sm-6 col-md-3">
      <div class="ls-box">
        <div class="ls-box-head">
          <span class="ls-ico-alone ls-ico-week"></span>
        </div>
        <div class="ls-box-body">
          <strong><?php echo date('d/m/Y', strtotime($r['Evento']['data'])) ?></strong>
          <small><h4><?php echo date('H:i:s', strtotime($r['Evento']['data'])) ?></h4></small>
        </div>
      </div>
    </div>
  </div>
</div>

<hr>

<?php } ?>