<a href="<?php echo Router::url(array( 'controller' => 'Departamentos', 'action' => 'inserir')) ?>">
	<button class="ls-btn ls-ico-plus">Criar Novo Departamento</button>
</a>
<?php # echo $this->Html->link('<button class="">Criar Novo Departamento</button>',array( 'controller' => 'Departamentos', 'action' => 'inserir'), array( 'escape' => false)) ?>

<!-- BOX DE FILTRO-->
<div class="ls-box-filter">
  <form action="" class="ls-form ls-form-inline ls-float-left">
    <label class="ls-label col-md-8 col-sm-8">
      <b class="ls-label-text">Status</b>
      <div class="ls-custom-select">
        <select id="dpstatus" class="ls-select">
        
          <option filtro="1" <?php if( $st == 1) echo 'selected="selected"' ?> >Ativos</option>
    
          <option filtro="0" <?php if( $st == 0) echo 'selected="selected"' ?>  >Desativados</option>
        
        </select>
      </div>
    </label>
  </form>
</div>
<!-- BOX DE FILTRO TERMINA-->

<!-- BOX DE FILTRO DE EXIBICAO -->



<table class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th>Descrição</th>
			<th>Pode Responder</th>
			<th>Pode Criar Usuário</th>
			<th>Criado Em</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $dps as $d ) { ?>
			<tr>
				<td><?php echo $d['Departamento']['descricao'] ?></td>
				<td><?php echo ( $d['Departamento']['can_reply'] == 1 ? 'Sim' : 'Não' ) ?></td>
				<td><?php echo ( $d['Departamento']['can_create_user'] == 1 ? 'Sim' : 'Não' ) ?></td>
				<td><?php echo date('d/m/Y',strtotime($d['Departamento']['created'])); ?></td>
				<td>
					<a href="<?php echo Router::url(array( 'controller' => 'Departamentos', 'action' => 'editar', $d['Departamento']['id'])) ?>">
						<button class="ls-btn ls-ico-pencil">Editar</button>
					</a>
					<?php echo $this->Form->postLink(
	                '<button class="ls-btn-danger ls-ico-remove">Excluir</button>',
	                array('action' => 'excluir', $d['Departamento']['id']),
	                array('confirm' => 'Deseja excluir o departamento?', 'escape' => false)); ?>	
				</td>
			</tr>
		<?php } ?>
	
	</tbody>
</table>
<hr>
<script type="text/javascript">
$(document).ready( function () {
	$("#dpstatus").change(function() {
		var parametro = $('option:selected', "#dpstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Departamentos/index/'+parametro;
	});
});
</script>