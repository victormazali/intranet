<?php echo $this->Form->create('Departamento', array( 'inputDefaults' => array( 'label' => false ))) ?>
<div class="ls-box-filter ls-form ls-form-inline ls-float-left">
<b class="ls-label-text">Nome do departamento:</b>
<?php echo $this->Form->input('descricao') ?>
<br>
<b class="ls-label-text">Departamento pode responder tickets:</b>
<br>
<label class="ls-label col-md-8 col-sm-8">
<div class="ls-custom-select">
<?php echo $this->Form->input('can_reply', array( 'options' => array( '0' => 'Não', '1' => 'Sim', ), array('class' => "ls-select") ) ) ?>
</div>
</label>
<br>
<br>
<br>
<b class="ls-label-text">Departamento pode criar usuários:</b>
<br>
<label class="ls-label col-md-8 col-sm-8 ls-form ls-form-inline ls-float-left">
<div class="ls-custom-select">
<?php echo $this->Form->input('can_create_user', array( 'options' => array( '0' => 'Não', '1' => 'Sim' ), array('class' => "ls-select") ) ) ?>
</div>
</label>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<hr>
<button class="ls-btn"">Salvar</button>
<?php echo $this->Form->end(); ?>
