<header class="ls-info-header">
      <h2 class="ls-title-3"> Formularios => Avaliações</h2>
</header>
<br>
<hr>
<br>
<table class="ls-table ls-table-striped">
  <thead>
    <tr>
      <th>Descrição</th>
      <th>Criado em</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach( $for as $d ) { ?>
      <tr>
        <td><?php echo $d['Formulario']['descricao'] ?></td>
        <td><?php echo date('d/m/Y',strtotime($d['Formulario']['created'])); ?></td>
        <td>
          <a href="<?php echo Router::url(array( 'controller' => 'Formularios', 'action' => 'convidar', $d['Form']['id'])) ?>">
            <button class="ls-btn ls-ico-user-add"></button>
          </a>
          <a href="<?php echo Router::url(array( 'controller' => 'Formularios', 'action' => 'editar', $d['Form']['id'])) ?>">
            <button class="ls-btn ls-ico-pencil"></button>
          </a>
          <?php echo $this->Form->postLink(
                  '<button class="ls-btn-danger ls-ico-remove"></button>',
                  array('action' => 'excluir', $d['Form']['id']),
                  array('confirm' => 'Deseja excluir o formulario?', 'class' => 'btn btn-danger', 'escape' => false)); ?> 
        </td>
      </tr>
    <?php } ?>
  
  </tbody>
</table>
<hr>