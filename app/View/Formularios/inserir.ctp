<style type="text/css">
/* Prevent the text contents of draggable elements from being selectable. */
[draggable] {
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  user-select: none;
  /* Required to make elements draggable in old WebKit */
  -khtml-user-drag: element;
  -webkit-user-drag: element;
}
.column {
  border: 2px solid #666666;
  -webkit-box-shadow: inset 0 0 3px #000;
  -ms-box-shadow: inset 0 0 3px #000;
  box-shadow: inset 0 0 3px #000;
  cursor: move;
}

.column.over {
  border: 2px dashed #000;
}


</style>

<?php echo $this->Form->create('Formulario', array( 'inputDefaults' => array( 'div' => false, 'label' => false))) ?>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="well">

      <label class="ls-label">
        <b class="ls-label-text">Categoria</b>
            <div class="ls-custom-select">
          <?php echo $this->Form->input('categorias', array( 'name' => 'data[Formulario][form_categoria_id]', 'options' => $categorias ), array( 'class' => 'ls-select')) ?>
          </div>
      </label>
      <label class="ls-label">
        <b class="ls-label-text">Título</b>
          <input type="text" name="data[Formulario][descricao]" placeholder="Título do Formulário">
      </label>3
      <div id="columns">
      <div id="perguntas">

      </div>
      </div>
      <button class="ls-btn ls-btn-primary" type="button" data-ls-module="modal" data-target="#addPerguntaModal" style="width:100%;">Adicionar Pergunta</button>
      <hr style="margin: 10px 0;">
      <button class="ls-btn ls-btn-dark" type="submit" style="width:100%;">Finalizar Formulário</button>
    </div>

  </div>
</div>
<?php echo $this->Form->end(); ?>

<div class="ls-modal" id="addPerguntaModal">
  <div class="ls-modal-box">
    <div class="ls-modal-header">
      <button data-dismiss="modal">&times;</button>
      <h4 class="ls-modal-title">Adicionar Pergunta</h4>
    </div>
    <div class="ls-modal-body">
      <form id="perguntas">

        <label class="ls-label">
          <b class="ls-label-text">Título</b>
            <input type="text" id="perguntaTitulo" placeholder="Título da Pergunta" required="required">
        </label>

        <label class="ls-label">
          <b class="ls-label-text">Descrição</b>
            <textarea type="text" id="perguntaDescricao" placeholder="Orientação sobre como responder a pergunta" required="required"></textarea>
        </label>

          <label class="ls-label">
            <b class="ls-label-text">Obrigatório</b>
            <div class="ls-custom-select">
              <select class="ls-select" id="perguntaObrigatorio">
                <option value="1">Sim</option>
                <option value="0">Não</option>
              </select>
            </div>
          </label>

          <label class="ls-label">
            <b class="ls-label-text">Tipo</b>
            <div class="ls-custom-select">
              <select class="ls-select" id="perguntaTipo" required="required">
                <option value="" disabled="disabled" selected>Selecione uma Opção</option>
                <option value="checkbox">Checkbox</option>
                <option value="radio">Radio</option>
                <option value="select">Select</option>
                <option value="text">Texto</option>
                <option value="date">Data</option>
                <option value="time">Time</option>
              </select>
            </div>
          </label>

          <div id="formularioCampos" class="ls-box" style="display:none; background-color:#fafafa;">
            <div class="row">
              <div class="col-md-12" id="opcoes">
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-md-offset-3" style="text-align:center;" >
                <button type="button" class="ls-btn ls-ico-minus" id="PerguntaOptionRemove"></button>
                <button type="button" class="ls-btn ls-ico-plus" id="PerguntaOptionAdd"></button>
              </div>
            </div>
          </div>

        </div>
        <div class="ls-modal-footer" style="text-align:right;">
          <button class="ls-btn" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="ls-btn-primary">Salvar</button>
        </div>
          </form>
      </div>
    </div>

    <script type="text/javascript">
      var qtdPerguntas = 0;
      var qtdOpcoes = 0;
      $("#perguntaTipo").change(function() {
        var valor = $(this).val();

        switch( valor ) { 

          case 'checkbox': 
          case 'radio': 
          case 'select': 
          $("#formularioCampos").show();
            break;

          case 'text': 
          case 'date': 
          case 'time': 
            $("#formularioCampos").hide();
            $("#formularioCampos").html('');
            break;
        }
      });
    
      $(document.body).on('submit', '#perguntas' ,function(e){
        qtdPerguntas++;
        var perguntaTipo = $("#perguntaTipo :selected").val();
        var perguntaTipoText = $("#perguntaTipo :selected").text();
        var perguntaDescricao = $("#perguntaDescricao").val();
        var perguntaTitulo = $("#perguntaTitulo").val();
        var perguntaObrigatorio = $("#perguntaObrigatorio").val();
        var optionsInputs = "";
        var perguntaOpcoes = $("#perguntaOpcoes").val();;

        $(".opcaoContainer").each(function( i, val ) {
          optionsInputs += '<input type="hidden" name="data[FormPergunta]['+qtdPerguntas+'][FormPerguntaOpcao]['+i+'][descricao] value='+val+'">';
        });

        if(perguntaObrigatorio == 1){
          var obg = '<a class="ls-tooltip-top ls-btn" aria-label="Sim">Obrigatório</a>';
        }else {
          var obg = '<a class="ls-tooltip-top ls-btn" disabled aria-label="Não">Obrigatório</a>';
        }
        

        var pergunta = '<div draggable="true" class="column" id="pergunta-'+qtdPerguntas+'" style="border:1px solid #ccc; border-radius:3px; margin:10px 0; padding:10px;">'
                      +'  <div class="row">'
                      +'    <div class="col-md-6">'
                      +'      <h3 class="title">'+perguntaTitulo+'</h3>'
                      +'    </div>'
                      +'    <div class="col-md-6" style="text-align:right;">'
                      +       perguntaTipoText
                      +'    </div>'
                      +'  </div>'
                      +'  <div style="clear:both;"></div>'
                      +'  <p>'+perguntaDescricao+'</p>'
                      +'  <p style="text-align:right;">'+obg+'</p>'
                      +'  <div class="content">'
                      +'  </div>'
                      +'  <div class="hidden">'
                      +'    <input type="hidden" name="data[FormPergunta]['+qtdPerguntas+'][label]" value="'+perguntaTitulo+'">'
                      +'    <input type="hidden" name="data[FormPergunta]['+qtdPerguntas+'][descricao]" value="'+perguntaDescricao+'">'
                      +'    <input type="hidden" name="data[FormPergunta]['+qtdPerguntas+'][tipo]" value="'+perguntaTipo+'">'
                      +'    <input type="hidden" name="data[FormPergunta]['+qtdPerguntas+'][obrigatorio]" value="'+perguntaObrigatorio+'">'
                      +'    <input type="hidden" name="data[FormPergunta]['+qtdPerguntas+'][Campo]" value="'+perguntaOpcoes+'">'
                      +'  </div>'
                      +'<div class="row">'
                      +'  <div class="col-md-12" style="text-align:right;">'
                      +'    <button type="button" class="ls-btn-danger perguntaRemover ls-ico-remove ls-ico-right">Remover </button>'
                      +'  </div>'
                      +' </div>'
                      +'   <div style="clear:both;"></div>'
                      +'</div>';

        $("#perguntas").append(pergunta);
        locastyle.modal.close();
        // Limpa campos para proxima pergunta
        $("#perguntaTipo").val('');
        $("#perguntaDescricao").val('');
        $("#perguntaTitulo").val('');
        e.preventDefault();
      });

      $(document.body).on('click', '.perguntaRemover' ,function(){
        $(this).parent().parent().parent().remove();
      });

      $("#PerguntaOptionAdd").click(function() {
        qtdOpcoes++;
        $("#opcoes").append( '<div id="opcao-'+qtdOpcoes+'" class="opcaoContainer" style="margin-bottom:5px;">'
                            +'<input type="text" id="perguntaOpcoes" placeholder="Descrição da Opção" required="required" style="width:100%;">'
                            +'</div>');
      });

      $("#PerguntaOptionRemove").click(function() {
        if( qtdOpcoes > 0 ) {
          $("#opcao-"+qtdOpcoes).remove();
          qtdOpcoes--;
        }
      });


var dragSrcEl = null;

function handleDragStart(e) {
  this.style.opacity = '0.4';  // this / e.target is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); // Necessary. Allows us to drop.
  }

  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.

  return false;
}

function handleDragEnter(e) {
  // this / e.target is the current hover target.
  this.classList.add('over');
}

function handleDragLeave(e) {
  this.classList.remove('over');  // this / e.target is previous target element.
}

function handleDrop(e) {
  // this / e.target is current target element.

  if (e.stopPropagation) {
    e.stopPropagation(); // Stops some browsers from redirecting.
  }

  // Don't do anything if dropping the same column we're dragging.
  if (dragSrcEl != this) {
    // Set the source column's HTML to the HTML of the columnwe dropped on.
    dragSrcEl.innerHTML = this.innerHTML;
    this.innerHTML = e.dataTransfer.getData('text/html');
  }

  return false;
}

function handleDragEnd(e) {
  // this/e.target is the source node.
  this.style.opacity = '1';
  [].forEach.call(cols, function (col) {
    col.classList.remove('over');
  });
}

var cols = document.querySelectorAll('#columns .column');
[].forEach.call(cols, function(col) {
  col.addEventListener('dragstart', handleDragStart, false);
  col.addEventListener('dragenter', handleDragEnter, false)
  col.addEventListener('dragover', handleDragOver, false);
  col.addEventListener('dragleave', handleDragLeave, false);
  col.addEventListener('drop', handleDrop, false);
  col.addEventListener('dragend', handleDragEnd, false);
});


    </script>