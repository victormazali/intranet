<div class="ls-steps">
  <div class="ls-steps-mobile">
    <a class="ls-steps-mobile-btn ls-ico-chevron-down"></a>
  </div>
  <ol class="ls-steps-nav">
    <li>
      <button class="ls-steps-btn" data-ls-module="steps" data-target="#step1" title="Titulo"></button>
    </li>
    <li>
      <button class="ls-steps-btn" data-ls-module="steps" data-target="#step2" title="Aparência"></button>
    </li>
    <li>
      <button class="ls-steps-btn" data-ls-module="steps" data-target="#step3" title="Conteúdo"></button>
    </li>
    <li>
      <button class="ls-steps-btn" data-ls-module="steps" data-target="#step4" title="Destinatários"></button>
    </li>
    <li>
      <button class="ls-steps-btn" data-ls-module="steps" data-target="#step5" title="Envios"></button>
    </li>
  </ol>
  <div class="ls-steps-main">
    <div class="ls-steps-content" id="step1">
      <p>Assumenda vitae voluptatem omnis exercitationem et magni. culpa mollitia nisi asperiores ut corrupti sit sed nihil id non aliquid eos reprehenderit. enim adipisci quam voluptas excepturi. vel laborum at nostrum nulla est a optio quia expedita. consequatur officiis accusamus doloremque quos reprehenderit quam assumenda occaecati ullam quis enim ipsum. odio quo et dolores dolorum dignissimos et voluptatem

Voluptatum mollitia assumenda illum vero consequatur delectus odit totam dolore repellendus consectetur iste atque. occaecati unde aut voluptate expedita pariatur inventore ut molestiae autem. quia repellendus aut veritatis enim neque et ratione

Quas tempora accusamus ut est et sunt et aut sunt cupiditate inventore. et molestias consequatur voluptas. voluptatibus autem natus tempora id quia harum corrupti. magnam et tempore perspiciatis architecto</p>
      <div class="ls-actions-btn">
        <a href="#" class="ls-btn-primary ls-float-right" data-action="next">Próximo</a>
      </div>
    </div>
    <div class="ls-steps-content" id="step2">
      <p>Et sint sint explicabo velit quia accusamus sunt asperiores. totam expedita ad voluptas. qui non omnis dolor. et architecto cumque officiis enim quo ut ut

Voluptatum voluptatem omnis quam quibusdam ducimus quo reprehenderit et voluptas eveniet ea natus accusamus assumenda. voluptas doloribus molestiae ut ipsam quia eum temporibus soluta nobis qui. laudantium quod deleniti eligendi ut quod tempore et. earum provident qui consequatur. quo at tempore magnam repellat quas magni ratione omnis animi est

Maxime non qui ducimus unde amet beatae laborum quidem ut sit. illum ducimus culpa modi sapiente quas laudantium excepturi. dolorem maxime similique eum ad quia sint aut est. accusantium ab provident molestiae. iusto eveniet aut et iusto et in rerum. alias et exercitationem dolorem explicabo ipsum sint ut voluptatem et voluptas eos vel voluptatem</p>
      <div class="ls-actions-btn">
        <a href="#" class="ls-btn" data-action="prev">Anterior</a>
        <a href="#" class="ls-btn-primary ls-float-right" data-action="next">Próximo</a>
      </div>
    </div>
    <div class="ls-steps-content" id="step3">
      <p>Illum quibusdam ut sit deleniti quas quia voluptates ab. dolorem at harum enim non nostrum deserunt dolorem. ut perspiciatis quos tempore molestias dolores accusamus numquam neque qui corporis autem. vel maiores itaque asperiores illo odit atque. tenetur dolores voluptas pariatur mollitia consequatur voluptates. iure nihil veniam in omnis doloribus maiores et iure

Eligendi voluptas odit maiores architecto qui quis voluptatibus nulla et optio. autem ducimus est rerum et reprehenderit cumque quia est. aut sunt molestiae assumenda voluptate delectus quod et. sunt mollitia inventore aut exercitationem praesentium rerum facere consectetur esse et similique numquam. quia adipisci quaerat quo sed id nostrum est alias inventore veritatis corporis aut quis. esse sequi in sit distinctio ex sunt quam inventore totam vitae. rem hic odit sed eum iste sit vero quis soluta

Asperiores error est perferendis recusandae ut eos debitis excepturi nobis autem. molestiae doloremque aut similique ipsa dolorem rem cumque nisi quia aut dolor. et voluptate ea sapiente. at recusandae veritatis sint nihil quisquam voluptatem sit amet sapiente velit rerum dolores nobis</p>
      <div class="ls-actions-btn">
        <a href="#" class="ls-btn" data-action="prev">Anterior</a>
        <a href="#" class="ls-btn-primary ls-float-right" data-action="next">Próximo</a>
      </div>
    </div>
    <div class="ls-steps-content" id="step4">
      <p>Et sapiente ut reiciendis aut iusto cum distinctio minus praesentium. repellat occaecati velit rerum unde ut illum voluptates nobis sit veritatis dicta sed. inventore a autem qui quidem odio ut earum officia ratione cumque rerum. voluptatem at et velit eum totam accusantium consequatur voluptatem nam consequatur. dolorem debitis repudiandae exercitationem. corporis ipsa qui eaque minus dolorem ea ea molestiae sed

Et est fugit vel ipsam qui qui suscipit. voluptas a sed vitae. quo voluptatum beatae maxime debitis. rerum ad est sit eum ut sed fuga. similique voluptatum magnam occaecati ipsam repellat quo ullam ut error laborum possimus aut

Dignissimos sed rerum mollitia excepturi praesentium aperiam nostrum provident quos sed nihil. est sint neque ab cumque distinctio consequatur facilis eveniet qui. accusantium sed atque est id quo velit nobis nulla</p>
      <div class="ls-actions-btn">
        <a href="#" class="ls-btn" data-action="prev">Anterior</a>
        <a href="#" class="ls-btn-primary ls-float-right" data-action="next">Próximo</a>
      </div>
    </div>
    <div class="ls-steps-content" id="step5">
      <p>Nisi sint aperiam fugit alias. minima labore modi eum et alias iusto ex. nam nemo quis et cumque quia atque blanditiis eum consequatur accusantium quia eum. qui maiores ipsam autem pariatur dolorum facere consequuntur sed eius rerum. est ut explicabo rerum non animi rerum odio iure veritatis nisi. recusandae id eligendi molestiae. minus sapiente incidunt dolorem hic excepturi nemo voluptatibus similique dolor illo labore ipsam aspernatur

Nesciunt laudantium ea omnis eveniet est ipsum consequatur. consectetur magni tenetur eos. quam quod et tempora. rerum eos fugiat autem qui ut. sed quia ex qui qui harum autem tenetur

Cupiditate a similique et dicta architecto et. quia placeat quod vero dolores pariatur aut. et ut rerum deserunt dolorem amet vel provident nulla quas ut est. quia ut nisi quae aut quia aut nesciunt aut sed doloremque rerum. dolores nobis natus non sed qui delectus enim magnam. dolore natus occaecati recusandae consequatur aut totam delectus sint amet ea sint ad</p>
      <div class="ls-actions-btn">
        <a href="#" class="ls-btn" data-action="prev">Anterior</a>
        <a href="#" class="ls-btn-primary ls-float-right">Finalizar</a>
      </div>
    </div>
  </div>
</div>

<?php echo $this->Form->create('Formulario', array( 'inputDefaults' => array( 'label' => false ))) ?>

<form action="" class="ls-form row">
  <fieldset>
    <label class="ls-label col-md-6">
      <b class="ls-label-text">Titulo do formulario</b>
      <?php echo $this->Form->input('descricao') ?>
    </label>

    <label class="ls-label col-md-6">
      <b class="ls-label-text">Quantas perguntas 'textarea' terá o formulario?</b>
      <?php echo $this->Form->input('text') ?>
    </label>

    <label class="ls-label col-md-6">
      <b class="ls-label-text">Quantas perguntas 'select' terá o formulario?</b>
      <?php echo $this->Form->input('select') ?>
    </label>

     <label class="ls-label col-md-6">
      <b class="ls-label-text">Quantas perguntas 'checkbox' terá o formulario?</b>
      <?php echo $this->Form->input('check') ?>
    </label>

     <label class="ls-label col-md-6">
      <b class="ls-label-text">Quantas perguntas 'radio' terá o formulario?</b>
      <?php echo $this->Form->input('radio') ?>
    </label>
  </fieldset>
  <div class="ls-actions-btn">
    <a class="ls-btn-primary ls-float-right" data-action="next">Próximo</a>
  </div>
  </form>
<?php echo $this->Form->end(); ?>


