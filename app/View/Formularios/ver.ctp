<header class="ls-info-header">
      <h2 class="ls-title-3"><?php echo $eventos['Evento']['descricao']?> => Time Line => (<?php echo $numberp?>) Posts</h2>
</header>
<?php echo $this->Form->create('Post', array( 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'postar'))) ?>
<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 1px;
                  
                          ">
<header class="ls-info-header">
    <h2 class="ls-title-3 ls-ico-user "><?php echo $this->Session->read('Usuario.nome') ?></h2>
    <p class="ls-float-right ls-float-none-xs ls-small-info">
   
    </p>
  </header>
<?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Comente algo...','style'=>'width:100%; margin: 3px;')) ?>
<br>
<p class="ls-float-right ls-float-none-xs ls-small-info">
      <a href="#" class="ls-btn-primary ls-ico-image" download></a>
      <button class="ls-btn-primary">Postar</button>
    </p>

</div>
<?php echo $this->Form->hidden('evento_id', array( 'value' => $eventos['Evento']['id'] )) ?>
<?php echo $this->Form->end(); ?>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<hr>
<br>
<br>


<?php foreach( $posts as $p)  { ?>

<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 5px;
                          ">
  <header class="ls-info-header">
    <h2 class="ls-title-3 ls-ico-user"><?php echo $p['Usuario']['nome'] ?></h2>
    <p class="ls-float-right ls-float-none-xs ls-small-info">
      Postado em <strong><?php echo date('d/m/Y', strtotime($p['Post']['created'])) ?> as <?php echo date('H:i:s', strtotime($p['Post']['created'])) ?></strong>
    </p>
  </header>

  <p><?php echo $p['Post']['conteudo'] ?></p>
  <br>

  <footer class="ls-info-header">
<?php echo $this->Form->create('Comentario', array( 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'comentar'))) ?>
  	<?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Escreva um comentario','style'=>'width:100%;')) ?>
  <?php echo $this->Form->hidden('post_id', array( 'value' => $p['Post']['id'] )) ?>
  <p class="ls-float-right ls-float-none-xs ls-small-info">
 <button class="ls-btn-primary">Comentar</button>
 </p>
<?php echo $this->Form->end(); ?>

  </footer>
  	<div id="box-toggle">
      <span class="openComentario">
        <p class="ls-cursor-pointer ls-txt-center ls-color-info">
          Ver Comentarios (<?php echo count($p['Comentario']) ?>)
        </p>
      </span>
			<div class="tgl comentario" style="display:none;">
          <br>
				 			<?php foreach( $p['Comentario'] as $c)  { ?>
							<div class="ls-box ls-xs-space ls-txt-left ">
              <p class="ls-float-right ls-float-none-xs ls-small-info">
              Postado em <strong><?php echo date('d/m/Y', strtotime($c['created'])) ?> as <?php echo date('H:i:s', strtotime($c['created'])) ?></strong>
              </p>
						  <h5 class="ls-title-5 ls-ico-user"> <?php echo $c['Usuario']['nome'] ?></h5>

						  <p> <?php echo $c['conteudo'] ?> </p>
							</div>
							<?php } ?>
					
			</div>
	</div>
</div>
<div class="container col-md-12 col-md-offset">
  <hr>
</div>

<?php } ?>



<?php echo $this->Form->end(); ?>

<script type="text/javascript">

$(document).ready(function(){
  $(".openComentario").click(function() {
    $(this).next().slideToggle()();
  });
})

</script>