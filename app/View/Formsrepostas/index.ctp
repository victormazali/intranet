<a href="<?php echo Router::url(array( 'controller' => 'Respostas', 'action' => 'inserir')) ?>">
	<button>Criar Novo Ticket</button>
</a>
<?php # echo $this->Html->link('<button class="">Criar Novo Departamento</button>',array( 'controller' => 'Departamentos', 'action' => 'inserir'), array( 'escape' => false)) ?>

<table>
	<thead>
		<tr>
			<th>Descrição</th>
			<th>Pode Responder</th>
			<th>Pode Criar Usuário</th>
			<th>Criado Em</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $dps as $d ) { ?>
			<tr>
				<td><?php echo $d['Reposta']['descricao'] ?></td>
				<td><?php echo ( $d['Resposta']['can_reply'] == 1 ? 'Sim' : 'Não' ) ?></td>
				<td><?php echo ( $d['Resposta']['can_create_user'] == 1 ? 'Sim' : 'Não' ) ?></td>
				<td><?php echo date('d/m/Y',strtotime($d['Resposta']['created'])); ?></td>
				<td>
					<a href="<?php echo Router::url(array( 'controller' => 'Respostas', 'action' => 'editar', $d['Resposta']['id'])) ?>">
						<button>Responder</button>
					</a>
					<?php echo $this->Form->postLink(
	                '<button>Encerrar</button>',
	                array('action' => 'excluir', $d['Resposta']['id']),
	                array('confirm' => 'Deseja enerrar o ticket?', 'class' => 'btn btn-danger', 'escape' => false)); ?>	
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>