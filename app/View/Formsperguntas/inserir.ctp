<?php echo $this->Form->create('Formspergunta', array( 'inputDefaults' => array( 'label' => false ), 'div' => false)) ?>
<form action="" class="ls-form ls-form-horizontal row">
  </form>
    <form action="" class="ls-form ls-form-horizontal row">
    <fieldset>
      <h2>Textarea</h2>
      <p class="ls-label-info">Perguntas do tipo textarea</p>
      <br>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Label</b>
        <p class="ls-label-info">Digite o nome da pergunta</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Maximo de caracteres</b>
        <?php echo $this->Form->input('textarea') ?>
      </label>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Ordem</b>
        <p class="ls-label-info">Ordem da pergunta no form</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
    </fieldset>
    </form>
<hr>
    <form action="" class="ls-form ls-form-horizontal row">
    <fieldset>
      <h2>Select</h2>
      <p class="ls-label-info">Perguntas do tipo select</p>
      <br>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Label</b>
        <p class="ls-label-info">Digite o nome da pergunta</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
      <label class="ls-label col-md-3">
         <div data-ls-module="collapse" data-target="#0" class="ls-collapse ">
    <a href="#" class="ls-collapse-header">
      <h3 class="ls-collapse-title">Campos</h3>
    </a>
    <div class="ls-collapse-body" id="0">
      <p>
       <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 1')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 2')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 3')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 4')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 5')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 6')) ?>
      </p>
    </div>
      </div>
      </label>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Ordem</b>
        <p class="ls-label-info">Ordem da pergunta no form</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
    </fieldset>
    </form>
<hr>
    <form action="" class="ls-form ls-form-horizontal row">
    <fieldset>
      <h2>Checkbox</h2>
      <p class="ls-label-info">Perguntas do tipo checkbox</p>
      <br>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Label</b>
        <p class="ls-label-info">Digite o nome da pergunta</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
      <label class="ls-label col-md-3">
          <div data-ls-module="collapse" data-target="#0" class="ls-collapse ">
    <a href="#" class="ls-collapse-header">
      <h3 class="ls-collapse-title">Campos</h3>
    </a>
    <div class="ls-collapse-body" id="0">
      <p>
       <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 1')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 2')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 3')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 4')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 5')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 6')) ?>
      </p>
    </div>
         </div>
      </label>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Ordem</b>
        <p class="ls-label-info">Ordem da pergunta no form</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
    </fieldset>
    </form>
<hr>
    <form action="" class="ls-form ls-form-horizontal row">
    <fieldset>
      <h2>Radio</h2>
      <p class="ls-label-info">Perguntas do tipo radio</p>
      <br>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Label</b>
        <p class="ls-label-info">Digite o nome da pergunta</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
      <label class="ls-label col-md-3">
        <div data-ls-module="collapse" data-target="#0" class="ls-collapse ">
    <a href="#" class="ls-collapse-header">
      <h3 class="ls-collapse-title">Campos</h3>
    </a>
    <div class="ls-collapse-body" id="0">
      <p>
       <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 1')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 2')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 3')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 4')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 5')) ?>
        <?php echo $this->Form->input('textarea', array('placeholder' => 'Campo 6')) ?>
      </p>
    </div>
  </div>
      </label>
      <label class="ls-label col-md-3">
        <b class="ls-label-text">Ordem</b>
        <p class="ls-label-info">Ordem da pergunta no form</p>
        <?php echo $this->Form->input('textarea') ?>
      </label>
    </fieldset>
    </form>
<hr>

<div class="ls-actions-btn">
    <button class="ls-btn">Salvar</button>
</div>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">

$(document).ready(function(){
  $(".openComentario").click(function() {
    $(this).next().slideToggle()();
  });
})

</script>