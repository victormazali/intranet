<!--
<a href="#" data-id="1" id="adicionarCampo">+ adicionar campo</a>
<form method="POST" action="gerarpdf.php" target="_blank">
    <div id="formulario">
        <input type="text" placeholder="Nº do Documento" maxlength="6" name="numeroDocumento" required/>
        <select name="tipoDocumento" required>
            <option value="" disabled selected>Tipo do Documento</option>
            <option value="01">Volvo</option>
            <option value="02">Saab</option>
        </select>
        <select name="subTipoDocumento" required>
            <option value="" disabled selected>Subtipo do Documento</option>
            <option value="01">Volvo</option>
            <option value="02">Saab</option>
        </select>
    </div>
    <input type="submit" value="Gerar Código"/>
</form>

<hr>
<hr>
<hr>
-->
<?php echo $this->Form->create('Formspergunta', array( 'inputDefaults' => array( 'label' => false ), 'div' => false)) ?>
<form action="" class="ls-form row">
    <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">Pergunta textarea ??</b>
      <?php echo $this->Form->textarea('descricao', array( 'style' => 'width:100%;', 'rows' => 5, 'data-ls-module' => 'charCounter', 'maxlength' => '1000')) ?>
    </label>
    </fieldset>
<hr>
    <fieldset>
 <label class="ls-label col-md-4">
      <b class="ls-label-text">Qual nota voce da?</b>
      <div class="ls-custom-select">
  <?php echo $this->Form->select('field',  array(1,2,3,4,5,6,7,8,9,10) );?>
      </div>
    </fieldset>
<hr>
    <fieldset>
       <label class="ls-label col-md-4">
      <b class="ls-label-text">Pergunta checkbox?</b>
      </label>
<?php 
    $options = array(
    'Value 1' => 'Satisfeito',
    'Value 2' => 'Regular',
    'Value 3' => 'Insatisfeito'
);

echo $this->Form->select('Model.field', $options, array(
    'multiple' => 'checkbox',
));

?>
    </fieldset>

<hr>
    <fieldset>
       <label class="ls-label col-md-4">
      <b class="ls-label-text">Pergunta radio?</b>
      </label>
<?php 
    $oradio = array(
    'Value 1' => 'Satisfeito',
    'Value 2' => 'Regular',
    'Value 3' => 'Insatisfeito'
);


$attributes = array('legend' => false, 'separator' =>'<br />');

echo $this->Form->radio('gender', $oradio, $attributes);

?>
    </fieldset>


<hr>






  <div class="ls-actions-btn">
    <button class="ls-btn">Proximo</button>
  </div>

</form>

<?php echo $this->Form->end(); ?>

<!--
<script type="text/javascript">
$(function() {
        var divContent = $('#formulario');
        var botaoAdicionar = $('a[data-id="1"]');
        var i = 1;

        //Ao clicar em adicionar ele cria uma linha com novos campos
        $(botaoAdicionar).click(function() {
                $('<div class="conteudoIndividual"><input type="text" placeholder="Nº do Documento" maxlength="6" name="numeroDocumento'+i+'" required/><select name="tipoDocumento'+i+'" required><option value="" disabled selected>Tipo do Documento</option><option value="01">Volvo</option><option value="02">Saab</option></select><select name="subTipoDocumento'+i+'" required><option value="" disabled selected>Subtipo do Documento</option><option value="01">Volvo</option><option value="02">Saab</option></select><a href="#" id="linkRemover">- Remover Campos</a></div>').appendTo(divContent);
                $('#removehidden').remove();
                i++;
                $('<input type="hidden" name="quantidadeCampos" value="'+i+'" id="removehidden">').appendTo(divContent);
        });

        //Cliquando em remover a linha é eliminada
        $('#linkRemover').live('click', function() { 
            $(this).parents('.conteudoIndividual').remove();
            i--;
        });
});
</script>

-->