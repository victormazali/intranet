<?php echo $this->Form->create('Departamento', array( 'inputDefaults' => array( 'label' => false ))) ?>
Nome do departamento:
<input type="text" name="data[Departamento][descricao]">
<?php #echo $this->Form->input('descricao') ?>

Departamento pode responder tickets:
<?php echo $this->Form->input('can_reply', array( 'options' => array( '0' => 'Não', '1' => 'Sim' ) ) ) ?>

Departamento pode criar usuários:
<?php echo $this->Form->input('can_create_user', array( 'options' => array( '0' => 'Não', '1' => 'Sim' ) ) ) ?>

<button type="submit">Editar</button>
<?php echo $this->Form->end(); ?>