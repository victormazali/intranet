<!DOCTYPE html>
<html class="ls-theme-light-blue">
  <head>
    <title>Intranet</title>

    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Insira aqui a descrição da página.">
    <link href="<?php echo $this->webroot ?>css/locastyle.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->webroot ?>/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="<?php echo $this->webroot ?>img/iconmirai.png" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo $this->webroot ?>img/iconmirai.png"" type="image/x-icon" />
    <!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    
    <!--<link href="<echo $this->webroot ?>css/cake.generic.css" rel="stylesheet">-->

    <link rel="icon" sizes="192x192" href="/locawebstyle/assets/images/ico-boilerplate.png">
    <link rel="apple-touch-icon" href="/locawebstyle/assets/images/ico-boilerplate.png">
  </head>
  <body>
    <div class="ls-topbar ">

  <!-- Barra de Notificações -->
  <div class="ls-notification-topbar">

    <!--  Links de apoio 
    <div class="ls-alerts-list">
      <a href="#" class="ls-ico-bell-o" data-counter="8" data-ls-module="topbarCurtain" data-target="#ls-notification-curtain"><span>Notificações</span></a>
     
      <a href="#" class="ls-ico-bullhorn" data-ls-module="topbarCurtain" data-target="#ls-help-curtain"><span>Ajuda</span></a>
      <a href="#" class="ls-ico-question" data-ls-module="topbarCurtain" data-target="#ls-feedback-curtain"><span>Sugestões</span></a> 
      
    </div>
   -->
    <!-- Dropdown com detalhes da conta de usuário -->
    <div data-ls-module="dropdown" class="ls-dropdown ls-user-account">
      <a href="#" class="ls-ico-user">
        <?php if($this->Session->read('Usuario.imagem') != NULL) { ?>
        <img src="<?php echo $this->webroot.$this->Session->read('Usuario.imagem') ?>" alt="" />
        <?php } ?>
        <span class="ls-name"><?php echo $this->Session->read('Usuario.nome') ?></span>
      </a>

      <nav class="ls-dropdown-nav ls-user-menu">
        <ul>
          <li><a class="ls-ico-cog" href="<?php echo Router::url(array( 'controller' => 'Usuarios', 'action' => 'editar', $this->Session->read('Usuario.id'))) ?>">Perfil</a></li>
          <li><a class="ls-ico-circle-left" href="<?php echo Router::url(array( 'controller' => 'Usuarios', 'action' => 'Logout')) ?>">Logout</a></li>
         </ul>
      </nav>
    </div>
  </div>

  <span class="ls-show-sidebar ls-ico-menu"></span>

  <a href="javascript:window.history.go(-1)"  class="ls-go-next"><span class="ls-text">Voltar</span></a>

  <!-- Nome do produto/marca com sidebar -->

   
    <h1 class="ls-brand-name">
      <a href="<?php echo Router::url(array( 'controller' => 'Pages', 'action' => 'display'))  ?>">
        <img title="Logo login" src="<?php echo $this->webroot ?>img/miraiintranet.png" width="180" style="margin-top: -5px;"/>
      </a>
    </h1>

  <!-- Nome do produto/marca sem sidebar quando for o pre-painel  -->
</div>


    <aside class="ls-sidebar">

  <div class="ls-sidebar-inner">
      <a href="#"  class="ls-go-prev"><span class="ls-text">Voltar à lista de serviços</span></a>

      <!-- if ($this->Departamento->hasPerm($this->Session->read('Usuario.departamento_id'),'can_reply') ){ -->

      <nav class="ls-menu">
        <ul>
          <li><a class="ls-ico-home" href="<?php echo Router::url(array( 'controller' => 'Pages', 'action' => 'display'))  ?>">Home</a></li>
          
          <li><a class="ls-ico-envelope" href="<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'index')) ?>">Tickets</a></li>

          <li><a class="ls-ico-calendar-check" href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'index')) ?>">Eventos</a></li>

          <li><a class="ls-ico-chart-bar-up" href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'lista')) ?>">Lista Pesquisas</a></li>
          <!--
          <li><a class="ls-ico-origins" href="<?php echo Router::url(array( 'controller' => 'Formularios', 'action' => 'index')) ?>">Formularios</a></li>
            -->
       <!--   <li><a class="ls-ico-table-alt" href="<?php echo Router::url(array( 'controller' => '#', 'action' => '#')) ?>">Matriz</a></li>

          
          <li>
            <a href="#" class="ls-ico-origins" title="Formularios">Questionarios</a>
            <ul>
              <li><a class="ls-ico-attachment" href="<?php echo Router::url(array( 'controller' => 'Forms',)) ?>">Avaliações</a></li>
              <li><a class="ls-ico-text" aria-hidden="false" href="<?php echo Router::url(array( 'controller' => 'Forms', 'action' => 'index')) ?>">Prospecções</a></li>
            </ul>
          </li>
        -->
          <?php if( $canAdmin == 1 ){ ?>
            <li>
              <a href="#" class="ls-ico-cog" title="Configurações">Admin</a>
              <ul>
                <li><a class="ls-ico-tree" href="<?php echo Router::url(array( 'controller' => 'Departamentos', 'action' => 'index')) ?>">Departamentos</a></li>

                <li><a class="ls-ico-users" href="<?php echo Router::url(array( 'controller' => 'Usuarios', 'action' => 'index')) ?>">Usuarios</a></li>

                <li><a class="ls-ico-calendar-check" href="<?php echo Router::url(array( 'controller' => 'Eventos', 'action' => 'adm')) ?>">Eventos</a></li>
                <!--
                <li><a class="ls-ico-origins" href="<?php echo Router::url(array( 'controller' => 'Formularios', 'action' => 'adm')) ?>">Formularios</a></li>
                -->
                <li><a class="ls-ico-star" href="<?php echo Router::url(array( 'controller' => 'Posthomes', 'action' => 'postdestaque')) ?>">Post Destaque</a>

                <li><a class="ls-ico-chart-bar-up" href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'index')) ?>">Pesquisas</a>
                <!--
                <li><a class="fa-wifi" href="<?php echo Router::url(array( 'controller' => 'Clientes', 'action' => 'index')) ?>">Wireless</a></li>
                -->
              </ul>
            </li>
          <?php } ?>
            
        </ul>
      </nav>

      
  </div>
</aside>


    <main class="ls-main ">
      <div class="container-fluid">
        <h1 class="ls-title-intro ls-ico-home"><?php echo ( $this->params['controller'] == 'Pages' ? 'Home' : $this->params['controller']) ?></h1>

      <div id="content">

      <?php if($this->Session->read('Usuario.email') == NULL) { ?>
      <div class="ls-alert-danger"><strong>Atenção!</strong> Cadastre um email para receber notificações e enviar tickets. <a href="<?php echo Router::url(array( 'controller' => 'Usuarios', 'action' => 'editar', $this->Session->read('Usuario.id'))) ?>">Clique aqui.</a></div>
      <?php } ?>

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>

      </div>
    </main>

    <aside class="ls-notification">
      <nav class="ls-notification-list" id="ls-notification-curtain" style="left: 1716px;">
        <h3 class="ls-title-2">Notificações</h3>
        <ul>
        <?php foreach( $notificacoes as $n ) { ?>
          <li class="ls-dismissable">
            <a href="<?php echo ( $n['Notificacao']['url'] ? $n['Notificacao']['url'] : "#" ); ?>"><?php echo $n['Notificacao']['descricao']?></a>  
            <a href="#" data-ls-module="dismiss" class="ls-ico-close ls-close-notification"></a>
          </li>
        <?php } ?>
        </ul>
      </nav>
      <!--
      <nav class="ls-notification-list" id="ls-help-curtain" style="left: 1756px;">
        <h3 class="ls-title-2">Feedback</h3>
        <ul>
          <li><a href="#">&gt; quo fugiat facilis nulla perspiciatis consequatur</a></li>
          <li><a href="#">&gt; enim et labore repellat enim debitis</a></li>
        </ul>
      </nav>

      <nav class="ls-notification-list" id="ls-feedback-curtain" style="left: 1796px;">
        <h3 class="ls-title-2">Ajuda</h3>
        <ul>
          <li class="ls-txt-center hidden-xs">
            <a href="#" class="ls-btn-dark ls-btn-tour">Fazer um Tour</a>
          </li>
          <li><a href="#">&gt; Guia</a></li>
          <li><a href="#">&gt; Wiki</a></li>
        </ul>
      </nav>
      -->
    </aside>
  
    <!-- We recommended use jQuery 1.10 or up -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://use.fontawesome.com/ca45973266.js"></script>

    <javascript src="<?php echo $this->webroot ?>js/meu.js">
    <javascript src="<?php echo $this->webroot ?>js/jquery.mask.js">
    <javascript src="<?php echo $this->webroot ?>js/bootstrap-filestyle.min.js">
    <?php 
    if( $this->action != 'convidar' )
      echo $this->Html->script('custom');
    ?>
    <?php echo $this->Html->script('locastyle') ?>

  </body>
</html>