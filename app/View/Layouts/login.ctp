<!DOCTYPE html>
<html class="ls-theme-gray">
<head>
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <title>Tela de Login</title>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <link href="<?php echo $this->webroot ?>css/locastyle.css" rel="stylesheet" type="text/css" />
</head>
<body class="documentacao documentacao_exemplos documentacao_exemplos_login-screen documentacao_exemplos_login-screen_index">

<div class="ls-login-parent">
  <div class="ls-login-inner">
    <div class="ls-login-container">
      <div class="ls-login-box">
  <h1 class="ls-login-logo">
    <img title="Logo login" src="<?php echo $this->webroot ?>img/mirai.png" /></h1>
    <?php echo $this->Session->flash() ?>
    <?php echo $this->fetch('content') ?>
</div>

    </div>
  </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot ?>js/jquery.mask.js"></script>
<script src="<?php echo $this->webroot ?>ls/locastyle.js" type="text/javascript"></script>

</body>
</html>
