<div class="container col-md-12 ">
  <header class="ls-info-header">
      <h2 class="ls-title-3 ">Criar Post Destaque</h2>
</header>
</div>

<?php echo $this->Form->create('Posthome', array( 'type' => 'file','inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'postdestaque'))) ?>
<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 0.5px;
                          ">

<?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Comente algo...','style'=>'width:100%; margin: 3px;')) ?>
<br>
<p class="ls-float-right ls-float-none-xs ls-small-info">
      <a href="#" class="ls-btn-primary ls-ico-image" download></a>
      <button class="ls-btn-primary">Postar</button>
    </p>
    <span class="ls-btn-primary">
 <?php echo $this->Form->input('img', array( 'type' => 'file')); ?>
  </span> 

</div>

<?php echo $this->Form->end(); ?>