<?php echo $this->Form->create('Ticket', array('type' => 'file', 'inputDefaults' => array( 'label' => false ))) ?>
<div class="ls-box-filter ls-form ls-float-left col-md-12 col-sm-12 col-lg-12">


	<label class="ls-label col-md-2">
	      <b class="ls-label-text">Categoria:</b>
	      <div class="ls-custom-select">
	      <?php echo $this->Form->select(
	    'categoria_id',
	     $categorias,
	    ['empty' => 'Categoria', 'class' => "ls-select"]
	);?>
	      </div>
	</label>
	<br>
	<label class="ls-label col-md-4">
	      <b class="ls-label-text">Enviar Ticket para</b>
	      <div class="ls-custom-select">
	      <?php echo $this->Form->input('destino', array('options' => array(1 => 'Somente Diretoria'), 'empty'=>'Diretoria e Recursos Humanos')); ?>
	      </div>
	</label>



<div class="container col-md-12">
  <hr>
</div>

	<label class="ls-label col-md-12">
	      <b class="ls-label-text">Título do Ticket:</b>
	      <?php echo $this->Form->textarea('titulo', array( 'style' => 'width:100%;', 'required' => true, 'rows' => 1, 'maxlength' => '60', 'data-ls-module' => 'charCounter' )) ?>
	</label>

	<label class="ls-label col-md-12">
	      <b class="ls-label-text">Conteudo do Ticket:</b>
	      <?php echo $this->Form->textarea('conteudo', array( 'style' => 'width:100%;', 'rows' => 5, 'required' => true )) ?>
	<?php echo $this->Form->input('doc', array( 'type' => 'file','style' => 'width:100%;')); ?>
	</label>


	<hr>
	<?php echo $this->Form->checkbox('anonimo', array('hiddenField' => 0)); ?> Enviar como anônimo
	<br>
	<br>
	<button class="ls-btn ls-ico-envelope col-md-3">Enviar</button>
</div>
<br>
<hr>
<?php echo $this->Form->end(); ?>


