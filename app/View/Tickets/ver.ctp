<header class="ls-info-header">
      <h2 class="ls-title-3"> Tickets =>  <?php echo $ticket['Categoria']['descricao'] ?> => <?php echo $ticket['Ticket']['titulo']?>
      	<?php if ($canMaster) { ?>
      				<?php if ($ticket['Ticket']['destino'] == 1 ) {?>
					<span class="ls-ico-star3 ls-color-warning"></span>
					<?php } else { ?>
					<span class="ls-ico-star3 ls-color-black"></span>
					<?php } ?>
		<?php } ?>
      </h2>
</header>
<b class="ls-label-text">Categoria: </b><?php echo $ticket['Categoria']['descricao'] ?>

<br>
<br>
<b class="ls-label-text">Titulo: </b>"<?php echo $ticket['Ticket']['titulo'] ?>"

<br>
<br>
<br>
<div class="ls-box">
<h5 class="ls-title-5"> <img src="<?php echo $this->webroot.$ticket['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" /> <?php echo $ticket['Usuario']['nome'] ?>:</h5>
 <p><?php echo nl2br(h($ticket['Ticket']['conteudo'])) ?></p>
 <?php if($ticket['Ticket']['documento'] != NULL) {?>
<a href="<?php echo $this->webroot.$ticket['Ticket']['documento'] ?>"  download>
            <button class="ls-ico-download ls-btn ls-float-right" download type="button">Baixar Documento</button>
</a>
<?php } else {?>
<a>
			<input type="button" value="Anexo vazio" disabled class="ls-btn ls-float-right">
</a>
<?php }?>

 <small>
Criado por: <?php echo $ticket['Usuario']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($ticket['Ticket']['created'])) ?>
		</small>
</div>

<hr>
<b class="ls-label-text">Movimentações:</b>
<br>
<br>


	<?php foreach( $respostas as $r)  { ?>
		<div class="ls-box">
			<h5 class="ls-title-5"> <img src="<?php echo $this->webroot.$r['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
	              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
	              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" /> <?php echo $r['Usuario']['nome'] ?>:</h5>
			<p><?php echo nl2br(h($r['Resposta']['conteudo'])) ?></p>
	 		<?php if($r['Resposta']['documento'] != NULL) {?>
				<a href="<?php echo $this->webroot.$r['Resposta']['documento'] ?>"  download>
	            <button class="ls-ico-download ls-btn ls-float-right" download type="button">Baixar Documento</button>
				</a>
			<?php } else {?>
				<a>
				<input type="button" value="Anexo vazio" disabled class="ls-btn ls-float-right">
				</a>
			<?php }?>
			<small>
			Respondido por: <?php echo $r['Usuario']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($r['Resposta']['created'])) ?>
			</small>
		</div>
	<?php } ?>


<hr>
<?php if ($ticket['Ticket']['status_id'] == 3 ) { ?>
		<b class="ls-label-text">Conclusão:</b>
		<br>
		<br>
		<div class="ls-box" style="-webkit-box-shadow: 0px 0px 12px -1px rgba(0,0,0,1);
		-moz-box-shadow: 0px 0px 12px -1px rgba(0,0,0,1);
		box-shadow: 0px 0px 12px -1px rgba(0,0,0,1);">
		<?php if($concluidos) {?>
		<h5 class="ls-title-5"> <img src="<?php echo $this->webroot.$concluidos['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
	              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
	              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" /> <?php echo $concluidos['Usuario']['nome'] ?>:</h5>
		<p><?php echo nl2br(h($concluidos['Resposta']['conteudo'])) ?></p>
		 	<?php if($concluidos['Resposta']['documento'] != NULL) {?>
			<a href="<?php echo $this->webroot.$concluidos['Resposta']['documento'] ?>"  download>
		            <button class="ls-ico-download ls-btn ls-float-right" download type="button">Baixar Documento</button>
			</a>
			<?php } else {?>
			<a>
					<input type="button" value="Anexo vazio" disabled class="ls-btn ls-float-right">
			</a>
			<?php }?>

		 	<small>
			Encerrado por: <?php echo $concluidos['Usuario']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($concluidos['Resposta']['created'])) ?>
			</small>
		<?php } ?>
		</div>


<?php } else { ?>
		<hr>
		<?php echo $this->Form->create('Resposta', array( 'type' => 'file', 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'responder'))) ?>
		<?php echo $this->Form->hidden('ticket_id', array( 'value' => $ticket['Ticket']['id'] )) ?>
		<b class="ls-label-text">Responder:</b>
		<br>
		<?php echo $this->Form->textarea('conteudo', array( 'style' => 'width:100%;', 'rows' => 5, 'required' => true )) ?>
		<?php echo $this->Form->input('doc', array( 'type' => 'file','style' => 'width:100%;')); ?>

		<button class="ls-btn ls-ico-envelope ls-float-right">Responder</button>
		<br>
		<hr>
		<?php echo $this->Form->end(); ?>

		<button data-ls-module="modal" data-target="#myAwesomeModal" class="ls-btn-danger ls-ico-remove">Encerrar Ticket</button>

		<div class="ls-modal" id="myAwesomeModal">
		  <div class="ls-modal-box">
		    <div class="ls-modal-header">
		      <button data-dismiss="modal">&times;</button>
		      <h4 class="ls-modal-title">Conclusão sobre o Ticket </h4>
		    </div>
		    <?php echo $this->Form->create('Resposta', array( 'type' => 'file', 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'concluir'))) ?>
		    <div class="ls-modal-body" id="myModalBody">
				<?php echo $this->Form->hidden('ticket_id', array( 'value' => $ticket['Ticket']['id'] )) ?>
				<?php echo $this->Form->textarea('conteudo', array( 'style' => 'width:100%;', 'rows' => 5, 'required' => true)) ?>
				<?php echo $this->Form->input('doc', array( 'type' => 'file','style' => 'width:100%;')); ?>
		    </div>
		    <div class="ls-modal-footer">
		      <button type="button" class="ls-btn ls-float-right" data-dismiss="modal">Cancelar</button>
		  	<button type="submit" class="ls-btn-danger ls-ico-remove ">Encerrar Ticket</button>
		    </div>
		    <?php echo $this->Form->end(); ?>
		  </div>
		</div><!-- /.modal -->
		
<?php } ?>

<br>

<hr>

<!--
		<?php echo $this->Form->postLink(
			                '<button class="ls-btn-danger ls-ico-remove">Encerrar Ticket</button>',
			                array('action' => 'excluir', $ticket['Ticket']['id']),
			                array('confirm' => 'Deseja encerrar o ticket?', 'class' => 'btn btn-danger', 'escape' => false)); ?>
		-->