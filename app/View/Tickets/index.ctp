<a href="<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'inserir')) ?>">
	<button class="ls-btn ls-ico-plus">Abrir Novo Ticket</button>
</a>

<!-- BOX DE FILTRO-->
<div class="ls-box-filter glyphicon">
  <form action="" class="ls-form ls-form-inline ls-float-left">
    <label class="ls-label col-md-8 col-sm-8">
      <b class="ls-label-text">Status</b>
      <div class="ls-custom-select">
        <select id="ticketstatus" class="ls-select">
        
          <option filtro="1" <?php if( $st == 1) echo 'selected="selected"' ?> >Ativos</option>
    
          <option filtro="0" <?php if( $st == 0) echo 'selected="selected"' ?>  >Encerrados</option>
        
        </select>
      </div>
    </label>
  </form>

  
</div>

		
<?php if ($canMaster) { ?>
		
	<table class="ls-table ">
		<thead>
			<tr>
				<th width="65px">Diretoria</th>
				<th>Titulo</th>
				<th>Por</th>
				<th>Inicio</th>
				<th>Última Atualização</th>
				<th>Categoria</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $tic as $t ) { ?>
				<?php if ($t['Ticket']['status_id'] == 0 ) { ?>
				<tr style="background: #FA5858 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } else if ($t['Ticket']['status_id'] == 1 ) { ?>
				<tr style="background: #F4FA58 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } else if ($t['Ticket']['status_id'] == 2 ) { ?>
				<tr style="background: #ffb380 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } else if ($t['Ticket']['status_id'] == 3 ) { ?>
				<tr style="background: #80ff80 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } ?>

					<?php if ($t['Ticket']['destino'] == 1 ) {?>
					<td class="ls-ico-star3 ls-txt-center ls-color-warning"></td>
					<?php } else { ?>
					<td class="ls-ico-star3 ls-txt-center"></td>
					<?php } ?>					
					<td><?php echo mb_strimwidth($t['Ticket']['titulo'],0,19,'...') ?></td>
					<td><?php echo $t['Usuario']['nome'] ?></td>	
					<td><?php echo date('d/m/Y',strtotime($t['Ticket']['created'])); ?></td>
					<td><?php echo date('d/m/Y',strtotime($t['Ticket']['updated'])); ?></td>
					<td><?php echo $t['Categoria']['descricao']?></td>
					<td><?php echo $t['Status']['descricao'] ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

<?php } else { ?>

	<table class="ls-table ">
		<thead>
			<tr>
				<th>Titulo</th>
				<th>Por</th>
				<th>Inicio</th>
				<th>Última Atualização</th>
				<th>Categoria</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $tic as $t ) { ?>
				<?php if ($t['Ticket']['status_id'] == 0 ) { ?>
				<tr style="background: #FA5858 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } else if ($t['Ticket']['status_id'] == 1 ) { ?>
				<tr style="background: #F4FA58 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } else if ($t['Ticket']['status_id'] == 2 ) { ?>
				<tr style="background: #ffb380 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } else if ($t['Ticket']['status_id'] == 3 ) { ?>
				<tr style="background: #80ff80 ; -webkit-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);
	box-shadow: 0px 0px 1px 0px rgba(0,0,0,0.75);" class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Tickets', 'action' => 'ver', $t['Ticket']['id'])) ?>';">
				<?php } ?>
					<td><?php echo mb_strimwidth($t['Ticket']['titulo'],0,19,'...') ?></td>
					<td><?php echo $t['Usuario']['nome'] ?></td>	
					<td><?php echo date('d/m/Y',strtotime($t['Ticket']['created'])); ?></td>
					<td><?php echo date('d/m/Y',strtotime($t['Ticket']['updated'])); ?></td>
					<td><?php echo $t['Categoria']['descricao'] ?></td>
					<td><?php echo $t['Status']['descricao'] ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>


<?php } ?>
<hr>

<script type="text/javascript">
$(document).ready( function () {
	$("#ticketstatus").change(function() {
		var parametro = $('option:selected', "#ticketstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Tickets/index/'+parametro;
	});
});
</script>