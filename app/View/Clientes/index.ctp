<table class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Email</th>
			<th>Telefone</th>
			<th>Efetuou login em</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $dps as $d ) { ?>
			<tr>
				<td><?php echo $d['Cliente']['nome'] ?></td>
				<td><?php echo $d['Cliente']['email'] ?></td>
				<td><?php echo $d['Cliente']['telefone'] ?></td>
				<td><?php echo date('d/m/Y h:i:s',strtotime($d['Cliente']['created'])); ?></td>		
			</tr>
		<?php } ?>
	
	</tbody>
</table>
<hr>
