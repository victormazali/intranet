<div class="ls-alert-box ls-dismissable ls-alert-box-success" role="alert">
  
    <span data-ls-module="dismiss" class="ls-dismiss">&times;</span>
  <?php echo $message ?>
</div>