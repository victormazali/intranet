<header class="ls-info-header">
      <h2 class="ls-title-3"> Pesquisas</h2>
</header>
<br>
<div class="ls-list">
  <header class="ls-list-header">
    <div class="ls-list-title col-md-9 ls-ico-text">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 1)) ?>" >Pergunta 1 </a> (<?php echo $perguntas1?>) Respostas
      <small><b>Qual tipo de problema, desconforto ou necessidade procura solucionar na compra de um veiculo novo?</b></small>
    </div>
    <div class="col-md-3 ls-txt-right">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 1)) ?>" class="ls-btn-primary">Administrar</a>
    </div>
  </header>
</div>
<div class="ls-list">
  <header class="ls-list-header">
    <div class="ls-list-title col-md-9 ls-ico-text">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 2)) ?>" >Pergunta 2 </a> (<?php echo $perguntas2?>) Respostas
      <small><b>Ao iniciar seu processo de compra quais são as etapas que encontra maior dificuldade?</b></small>
    </div>
    <div class="col-md-3 ls-txt-right">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 2)) ?>" class="ls-btn-primary">Administrar</a>
    </div>
  </header>
</div>
<div class="ls-list">
  <header class="ls-list-header">
    <div class="ls-list-title col-md-9 ls-ico-text">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 3)) ?>" >Pergunta 3 </a> (<?php echo $perguntas3?>) Respostas
      <small><b>Durante sua experiência de compra o que fez sentir mal, frustrado ou que tem dado maiores transtornos?</b></small>
    </div>
    <div class="col-md-3 ls-txt-right">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 3)) ?>" class="ls-btn-primary">Administrar</a>
    </div>
  </header>
</div>
<div class="ls-list">
  <header class="ls-list-header">
    <div class="ls-list-title col-md-9 ls-ico-text">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 4)) ?>" >Pergunta 4 </a> (<?php echo $perguntas4?>) Respostas
      <small><b>Para tornar essa experiência de compra mais agradável quais as melhorias poderiam ser adotadas?</b></small>
    </div>
    <div class="col-md-3 ls-txt-right">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 4)) ?>" class="ls-btn-primary">Administrar</a>
    </div>
  </header>
</div>
<div class="ls-list">
  <header class="ls-list-header">
    <div class="ls-list-title col-md-9 ls-ico-text">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 5)) ?>" >Pergunta 5 </a> (<?php echo $perguntas5?>) Respostas
      <small><b>Quais os fatores que provocam incômodos na hora de realizar manutenção, reparo ou garantia junto a nossa assistência técnica?</b></small>
    </div>
    <div class="col-md-3 ls-txt-right">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 5)) ?>" class="ls-btn-primary">Administrar</a>
    </div>
  </header>
</div>
<div class="ls-list">
  <header class="ls-list-header">
    <div class="ls-list-title col-md-9 ls-ico-text">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 6)) ?>" >Pergunta 6 </a> (<?php echo $perguntas6?>) Respostas
      <small><b>O que poderíamos realizar para que sua satisfação como nossa assistência técnica seja plena?</b></small>
    </div>
    <div class="col-md-3 ls-txt-right">
      <a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'verperguntas', 6)) ?>" class="ls-btn-primary">Administrar</a>
    </div>
  </header>
</div>

<hr>
<br>
<table class="ls-table ls-table-striped ls-table-bordered">
  <thead>
    <tr>
      <th>Nome</th>
      <th>E-mail</th>
      <th>Telefone</th>
      <th>Profissão</th>
      <th>Estado Civil</th>
      <th>Enviado em</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach( $dps as $d ) { ?>
      <tr class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $d['Pesquisa']['id'])) ?>';">
        <td><?php echo mb_strimwidth($d['Pesquisa']['nome'],0,21,'...') ?></td>
        <td><?php echo mb_strimwidth($d['Pesquisa']['email'],0,21,'...') ?></td>
        <td><?php echo $d['Pesquisa']['telefone'] ?></td>
        <td><?php echo mb_strimwidth($d['Pesquisa']['profissao'],0,21,'...') ?></td>
        <td><?php echo mb_strimwidth($d['Estado']['descricao'],0,21,'...') ?></td>
        <td><?php echo date('d/m/Y',strtotime($d['Pesquisa']['created'])); ?> as <?php echo date('H:i:s',strtotime($d['Pesquisa']['created'])); ?> </td>
      </tr>
    <?php } ?>
  
  </tbody>
</table>
<hr>

<!--
<tbody>
    <?php foreach( $dps as $d ) { ?>
      <tr class="ls-cursor-pointer" onclick="document.location = '<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $d['Pesquisa']['id'])) ?>';">
        <td><?php echo mb_strimwidth($d['Pesquisa']['nome'],0,21,'...') ?></td>
        <td><?php echo mb_strimwidth($d['Pesquisa']['email'],0,21,'...') ?></td>
        <td><?php echo $d['Pesquisa']['telefone'] ?></td>
        <td>
        <?php if ($d['Pesquisa']['pergunta1']) { ?>
        <span class="ls-ico-radio-checked ls-color-info"></span>
        <?php }else { ?>
        <span class="ls-ico-radio-unchecked"></span>
        <?php } ?>
        
        <?php if ($d['Pesquisa']['pergunta2']) { ?>
        <span class="ls-ico-radio-checked ls-color-info"></span>
        <?php }else { ?>
        <span class="ls-ico-radio-unchecked"></span>
        <?php } ?>
        
        <?php if ($d['Pesquisa']['pergunta3']) { ?>
        <span class="ls-ico-radio-checked ls-color-info"></span>
        <?php }else { ?>
        <span class="ls-ico-radio-unchecked"></span>
        <?php } ?>
        
        <?php if ($d['Pesquisa']['pergunta4']) { ?>
        <span class="ls-ico-radio-checked ls-color-info"></span>
        <?php }else { ?>
        <span class="ls-ico-radio-unchecked"></span>
        <?php } ?>
        
        <?php if ($d['Pesquisa']['pergunta5']) { ?>
        <span class="ls-ico-radio-checked ls-color-info"></span>
        <?php }else { ?>
        <span class="ls-ico-radio-unchecked"></span>
        <?php } ?>
        
        <?php if ($d['Pesquisa']['pergunta6']) { ?>
        <span class="ls-ico-radio-checked ls-color-info"></span>
        <?php }else { ?>
        <span class="ls-ico-radio-unchecked"></span>
        <?php } ?>
       
        </td>
        <td><?php echo date('d/m/Y',strtotime($d['Pesquisa']['created'])); ?> as <?php echo date('H:i:s',strtotime($d['Pesquisa']['created'])); ?> </td>
      </tr>
    <?php } ?>
  
  </tbody>
-->