<div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 ls-border" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
<?php switch ($pergunta) {
			case 1: ?>
			<header class="ls-info-header">
				<h2 class="ls-title-3">1-	Qual tipo de problema, desconforto ou necessidade que procura solucionar ou atender na compra de um veículo novo?</h2>
				<h4 class="ls-txt-center">
				<br>
				<b>(<?php echo $perguntas1 ?>) Respostas</b>
				<br>
				...
				</h4>
			</header>
			<?php foreach( $perguntas as $p)  { 
				if ($p['Pesquisa']['pergunta1']) { ?>

					<div class="ls-box">
					<p><?php echo nl2br(h($p['Pesquisa']['pergunta1'])) ?></p>
						<a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $p['Pesquisa']['id'])) ?>">
	            		<button class="ls-ico-book ls-btn ls-float-right"  type="button">Ver Completo</button>
						</a>
					<small>
						Enviado por: <?php echo $p['Pesquisa']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($p['Pesquisa']['created'])) ?>
					</small>
					</div>
					<hr>
				<?php } 
					}
				break;
				// FIM -------------------------------------------------
			case 2: ?>
			<header class="ls-info-header">
				<h2 class="ls-title-3">2-	Ao iniciar seu processo de compra em quais etapas encontra maior dificuldade?</h2>
				<h4 class="ls-txt-center">
				<br>
				<b>(<?php echo $perguntas2 ?>) Respostas</b>
				<br>
				...
				</h4>
			</header>
			<?php foreach( $perguntas as $p)  { 
				if ($p['Pesquisa']['pergunta2']) { ?>
					<div class="ls-box">
					<p><?php echo nl2br(h($p['Pesquisa']['pergunta2'])) ?></p>
						<a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $p['Pesquisa']['id'])) ?>">
	            		<button class="ls-ico-book ls-btn ls-float-right"  type="button">Ver Completo</button>
						</a>
					<small>
						Enviado por: <?php echo $p['Pesquisa']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($p['Pesquisa']['created'])) ?>
					</small>
					</div>
					<hr>
				<?php } 
			}
				break;
				// FIM -------------------------------------------------
			case 3: ?>
			<header class="ls-info-header">
				<h2 class="ls-title-3">3-	Durante sua experiência de compra algo o desmotiva, causa desconforto ou frustração?</h2>
				<h4 class="ls-txt-center">
				<br>
				<b>(<?php echo $perguntas3 ?>) Respostas</b>
				<br>
				...
				</h4>
			</header>
			<?php foreach( $perguntas as $p)  { 
				if ($p['Pesquisa']['pergunta3']) { ?>
					<div class="ls-box">
					<p><?php echo nl2br(h($p['Pesquisa']['pergunta3'])) ?></p>
						<a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $p['Pesquisa']['id'])) ?>">
	            		<button class="ls-ico-book ls-btn ls-float-right"  type="button">Ver Completo</button>
						</a>
					<small>
						Enviado por: <?php echo $p['Pesquisa']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($p['Pesquisa']['created'])) ?>
					</small>
					</div>
					<hr>
				<?php } 
			}
				break;
				// FIM -------------------------------------------------
			case 4: ?>
			<header class="ls-info-header">
				<h2 class="ls-title-3">4-	Para tornar a experiência de compra mais agradável quais melhorias poderiam ser adotadas?</h2>
				<h4 class="ls-txt-center">
				<br>
				<b>(<?php echo $perguntas4 ?>) Respostas</b>
				<br>
				...
				</h4>
			</header>
			<?php foreach( $perguntas as $p)  { 
				if ($p['Pesquisa']['pergunta4']) { ?>
					<div class="ls-box">
					<p><?php echo nl2br(h($p['Pesquisa']['pergunta4'])) ?></p>
						<a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $p['Pesquisa']['id'])) ?>">
	            		<button class="ls-ico-book ls-btn ls-float-right"  type="button">Ver Completo</button>
						</a>
					<small>
						Enviado por: <?php echo $p['Pesquisa']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($p['Pesquisa']['created'])) ?>
					</small>
					</div>
					<hr>
				<?php } 
			}
				break;
				// FIM -------------------------------------------------
			case 5: ?>
			<header class="ls-info-header">
				<h2 class="ls-title-3">5-	Quais fatores provocam incômodos na hora de realizar manutenção, reparo ou utilizar a garantia junto à nossa Assistência Técnica?</h2>
				<h4 class="ls-txt-center">
				<br>
				<b>(<?php echo $perguntas5 ?>) Respostas</b>
				<br>
				...
				</h4>
			</header>
			<?php foreach( $perguntas as $p)  { 
				if ($p['Pesquisa']['pergunta5']) { ?>
					<div class="ls-box">
					<p><?php echo nl2br(h($p['Pesquisa']['pergunta5'])) ?></p>
						<a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $p['Pesquisa']['id'])) ?>">
	            		<button class="ls-ico-book ls-btn ls-float-right"  type="button">Ver Completo</button>
						</a>
					<small>
						Enviado por: <?php echo $p['Pesquisa']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($p['Pesquisa']['created'])) ?>
					</small>
					</div>
					<hr>
				<?php } 
			}
				break;
				// FIM -------------------------------------------------
			case 6: ?>
			<header class="ls-info-header">
				<h2 class="ls-title-3">6-	O que poderíamos realizar para que sua satisfação, com nossa Assistência Técnica, seja plena?</h2>
				<h4 class="ls-txt-center">
				<br>
				<b>(<?php echo $perguntas6 ?>) Respostas</b>
				<br>
				...
				</h4>
			</header>
			<?php foreach( $perguntas as $p)  { 
				if ($p['Pesquisa']['pergunta6']) { ?>
					<div class="ls-box">
					<p><?php echo nl2br(h($p['Pesquisa']['pergunta6'])) ?></p>
						<a href="<?php echo Router::url(array( 'controller' => 'Pesquisas', 'action' => 'ver', $p['Pesquisa']['id'])) ?>">
	            		<button class="ls-ico-book ls-btn ls-float-right"  type="button">Ver Completo</button>
						</a>
					<small>
						Enviado por: <?php echo $p['Pesquisa']['nome'] ?> em <?php echo date('d/m/Y H:i:s', strtotime($p['Pesquisa']['created'])) ?>
					</small>
					</div>
					<hr>
				<?php } 
			}
				break;
		} ?>
</div>
</div>