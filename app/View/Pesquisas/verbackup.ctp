<header class="ls-info-header">
      <h2 class="ls-title-3"> Pesquisa =>  <?php echo $ticket['Pesquisa']['nome'] ?></h2>
</header>
<div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 ls-border" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
<?php echo $this->Form->create('Pesquisa', array( 'inputDefaults' => array( 'label' => false ))) ?>
<?php echo $this->Form->hidden('id') ?>
<div class="ls-form-horizontal">
<div class="row">
<label class="ls-label col-md-6">
	    <b class="ls-label-text">Nome:</b>
<?php echo $this->Form->input('nome',array(	 'disabled' => 'disabled'	)) ?> 
</label>
<label class="ls-label col-md-6">
	    <b class="ls-label-text">CPF:</b>
<?php echo $this->Form->input('cpf',array(	 'disabled' => 'disabled'	)) ?> 
</label>
</div>
<div class="row">
<label class="ls-label col-md-6">
	    <b class="ls-label-text">E-mail:</b>
<?php echo $this->Form->input('email',array(	 'disabled' => 'disabled'	)) ?>
</label> 
<label class="ls-label col-md-6">
	    <b class="ls-label-text">Telfone:</b>
<?php echo $this->Form->input('telefone',array(	 'disabled' => 'disabled'	)) ?> 
</label>
</div>
</div>
<hr>
<label class="ls-label col-md-12">
	    <b class="ls-label-text">1- Qual tipo de problema, desconforto ou necessidade que procura solucionar ou atender na compra de um veículo novo?</b>
<div class="ls-box">
					<p><?php echo nl2br(h($ticket['Pesquisa']['pergunta1'])) ?></p>
</div> 
</label>

<label class="ls-label col-md-12">
	    <b class="ls-label-text">2-	Ao iniciar seu processo de compra em quais etapas encontra maior dificuldade?</b>
<div class="ls-box">
					<p><?php echo nl2br(h($ticket['Pesquisa']['pergunta2'])) ?></p>
</div> 
</label>

<label class="ls-label col-md-12">
	    <b class="ls-label-text">3- Durante sua experiência de compra algo o desmotiva, causa desconforto ou frustração?</b>
<div class="ls-box">
					<p><?php echo nl2br(h($ticket['Pesquisa']['pergunta3'])) ?></p>
</div> 
</label>

<label class="ls-label col-md-12">
	    <b class="ls-label-text">4-	Para tornar a experiência de compra mais agradável quais melhorias poderiam ser adotadas?</b>
<div class="ls-box">
					<p><?php echo nl2br(h($ticket['Pesquisa']['pergunta4'])) ?></p>
</div> 
</label>

<label class="ls-label col-md-12">
	    <b class="ls-label-text">5-	Quais fatores provocam incômodos na hora de realizar manutenção, reparo ou utilizar a garantia junto à nossa Assistência Técnica?</b>
<div class="ls-box">
					<p><?php echo nl2br(h($ticket['Pesquisa']['pergunta5'])) ?></p>
</div> 
</label>

<label class="ls-label col-md-12">
	    <b class="ls-label-text">6-	O que poderíamos realizar para que sua satisfação, com nossa Assistência Técnica, seja plena?</b>
<div class="ls-box">
					<p><?php echo nl2br(h($ticket['Pesquisa']['pergunta6'])) ?></p>
</div> 
</label>
<?php echo $this->Form->end(); ?>

<hr>

</div>
</div>
