<br>
<br>
<br>
<a data-ls-module="modal" data-target="#myAwesomeModal" class="ls-cursor-pointer">
  <h1 class="ls-txt-center">
    <img title="Logo login" src="<?php echo $this->webroot ?>img/mirai.png" />
  </h1>
</a>
<br>

<hr>
<?php echo $this->Form->create('Pesquisa', array('inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'inserir'))) ?>
  <div class="ls-form-horizontal">
    <fieldset>
      <label class="ls-label col-md-6">
        <b class="ls-label-text">Nome</b>
        <?php echo $this->Form->input('nome', array( 'placeholder' => 'Nome e Sobrenome', 'title' => 'Div Title', 'required' => true)) ?>
      </label>
      <label class="ls-label col-md-6">
        <b class="ls-label-text">CPF</b>
        <?php echo $this->Form->input('cpf', array( 'data-mask'  => "999.999.999-99", 'placeholder' => 'Somente números', 'required' => true)) ?>
      </label>
      <label class="ls-label col-md-6">
        <b class="ls-label-text">E-mail</b>
        <?php echo $this->Form->input('email', array( 'placeholder' => 'Seu e-mail', 'required' => true)) ?>
      </label>
      <label class="ls-label col-md-6">
        <b class="ls-label-text">Celular</b>
        <?php echo $this->Form->input('telefone', array( 'data-mask'  => "(99) 99999-9999", 'placeholder' => 'Seu telefone celular', 'required' => true)) ?>
      </label>
    </fieldset>
  </div>

  <hr>

  <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">1 - Qual tipo de problema, desconforto ou necessidade procura solucionar na compra de um veiculo novo?</b>
      <p class="ls-label-info">Descrição da pergunta</p>
      <?php echo $this->Form->textarea('pergunta1', array( 'style' => 'width:100%;', 'rows' => 3)) ?>
    </label>
  </fieldset>

  <hr>

  <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">2 - Ao iniciar seu processo de compra quais são as etapas que encontra maior dificuldade?</b>
      <p class="ls-label-info">Descrição da pergunta</p>
      <?php echo $this->Form->textarea('pergunta2', array( 'style' => 'width:100%;', 'rows' => 3)) ?>
    </label>
  </fieldset>

  <hr>

  <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">3 - Durante sua experiência de compra o que fez sentir mal, frustrado ou que tem dado maiores transtornos?</b>
      <p class="ls-label-info">Descrição da pergunta</p>
      <?php echo $this->Form->textarea('pergunta3', array( 'style' => 'width:100%;', 'rows' => 3)) ?>
    </label>
  </fieldset>

  <hr>

  <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">4 - Para tornar essa experiência de compra mais agradável quais as melhorias poderiam ser adotadas?</b>
      <p class="ls-label-info">Descrição da pergunta</p>
      <?php echo $this->Form->textarea('pergunta4', array( 'style' => 'width:100%;', 'rows' => 3)) ?>
    </label>
  </fieldset>

  <hr>

  <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">5 - Quais os fatores que provocam incômodos na hora de realizar manutenção, reparo ou garantia junto a nossa assistência técnica?</b>
      <p class="ls-label-info">Descrição da pergunta</p>
      <?php echo $this->Form->textarea('pergunta5', array( 'style' => 'width:100%;', 'rows' => 3)) ?>
    </label>
  </fieldset>

  <hr>

  <fieldset>
    <label class="ls-label col-md-12">
      <b class="ls-label-text">6 - O que poderíamos realizar para que sua satisfação como nossa assistência técnica seja plena?</b>
      <p class="ls-label-info">Descrição da pergunta</p>
      <?php echo $this->Form->textarea('pergunta6', array( 'style' => 'width:100%;', 'rows' => 3)) ?>
    </label>
  </fieldset>

  
  <div class="ls-actions-btn">
   
  </div>


<div class="ls-modal" id="myAwesomeModal">
  <div class="ls-modal-box">
    <div class="ls-modal-header">
      <button data-dismiss="modal">&times;</button>
      <h4 class="ls-modal-title"> <h1 class="ls-txt-center">
    <img title="Logo login" src="<?php echo $this->webroot ?>img/mirai.png" /></h1></h4>
    </div>
    <div class="ls-modal-body" id="myModalBody">
<p class="ls-text-md">Você é uma pessoa respeitada e admirada em nossa comunidade, razão pela qual aqueles que estão à sua volta ouvem seus conselhos, suas histórias e suas sugestões.<br>
A Mirai Motors, da mesma forma, valoriza o que Vossa Senhoria representa e, diante disso, pedimos licença para nos ajudar a responder algumas perguntas.<br>
Antes, porém, gostaríamos de informar o motivo de tais questionamentos. 
Iniciamos um processo de melhoria e inovação na comercialização de nossos produtos e serviços, buscando melhorar sua experiência de compra, manutenção e recompra do seu veículo Toyota.

</p>

<p><b>Após sua valiosa colaboração e como forma de agradecê-lo(a), iremos presenteá-lo(a) com um brinde personalizado e ainda participará de um sorteio cujo prêmio será R$1.200,00 (mil e duzentos reais) em combustível. Após o envio das respostas encaminharemos um e-mail com um voucher para retirar o brinde e o número do seu cupom para participar do sorteio. </b></p>
    </div>
    <div class="ls-modal-footer">
      <button class="ls-btn ls-float-right" data-dismiss="modal">Responder</button>
      <br>
      <br>
    </div>
  </div>
</div><!-- /.modal -->

 <button class="ls-btn">Enviar</button>
<?php echo $this->Form->end(); ?>
<hr>

<script type="text/javascript">

document.addEventListener("DOMContentLoaded", function() {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Por favor. Preencha seus dados pessoais para enviar a pesquisa");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})
</script>