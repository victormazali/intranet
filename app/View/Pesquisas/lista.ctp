<header class="ls-info-header">
      <h2 class="ls-title-3">Lista Pesquisas</h2>
</header>
<br>
<hr>
<br>
<table class="ls-table ls-table-striped ls-table-bordered">
  <thead>
    <tr>
      <th>Nome</th>
      <th width="100">Telefone</th>
      <th>E-mail</th>
      <th>Cupom</th>
      <th width="45">Brinde</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach( $dps as $d ) { ?>
      <tr>
        <td><?php echo$d['Pesquisa']['nome'] ?></td>
        <td><?php echo $d['Pesquisa']['telefone']; ?></td>
        <td><?php echo $d['Pesquisa']['email']?></td>
        <td>http://miraimotors.com.br/intranet/<?php echo $d['Pesquisa']['cupom']?></td>
        <td style="width=10px;" >
        <?php if($d['Pesquisa']['brinde'] == 1) { ?>
        <a href="#" class="ls-btn ls-ico-checkmark" disabled="disabled"></a>
        <?php } else { ?>
          <?php echo $this->Form->postLink(
                  '<button class="ls-btn ls-ico-checkbox-unchecked"></button>',
                  array('controller' => 'Pesquisas', 'action' => 'confirmar', $d['Pesquisa']['id']),
                  array('confirm' => 'Deseja confirmar entrega do brinde?', 'escape' => false)); ?>
          <?php } ?>
        </td>
      </tr>
    <?php } ?>
  
  </tbody>
</table>
<hr>