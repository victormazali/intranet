<a href="<?php echo Router::url(array( 'controller' => 'Usuarios', 'action' => 'inserir')) ?>">
	<button class="ls-btn ls-ico-user-add">Criar Novo Usuario</button>
</a>
<?php # echo $this->Html->link('<button class="">Criar Novo Departamento</button>',array( 'controller' => 'Departamentos', 'action' => 'inserir'), array( 'escape' => false)) ?>

<!-- BOX DE FILTRO-->
<div class="ls-box-filter">
  <form action="" class="ls-form ls-form-inline ls-float-left">
    <label class="ls-label col-md-8 col-sm-8">
      <b class="ls-label-text">Status</b>
      <div class="ls-custom-select">
        <select id="userstatus" class="ls-select">
        
          <option filtro="1" <?php if( $st == 1) echo 'selected="selected"' ?> >Ativos</option>
    
          <option filtro="0" <?php if( $st == 0) echo 'selected="selected"' ?>  >Desativados</option>
        
        </select>
      </div>
    </label>
  </form>

  
</div>



<table id="" class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th>Nome</th>
			<th>CPF</th>
			<th>Departamento</th>
			<th>Criado em</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $users as $u ) { ?>
			<tr>
				<td class="ls-ico-user"> <?php echo $u['Usuario']['nome'] ?></td>
				<td><?php echo $u['Usuario']['cpf'] ?></td>
				<td><?php echo $u['Departamento']['descricao'] ?></td>
				<td><?php echo date('d/m/Y',strtotime($u['Usuario']['created'])); ?></td>
				<td>
					<a href="<?php echo Router::url(array( 'controller' => 'Usuarios', 'action' => 'editar', $u['Usuario']['id'])) ?>">
						<button class="ls-btn ls-ico-pencil">Editar</button>
					</a>
					<?php echo $this->Form->postLink(
	                '<button class="ls-btn-danger ls-ico-remove">Desativar</button>',
	                array('action' => 'excluir', $u['Usuario']['id']),
	                array('confirm' => 'Deseja excluir o usuario?', 'escape' => false)); ?>	
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<hr>
<script type="text/javascript">
$(document).ready( function () {
	$("#userstatus").change(function() {
		var parametro = $('option:selected', "#userstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Usuarios/index/'+parametro;
	});
});
</script>