<?php echo $this->Form->create('Usuario', array('type' => 'file', 'inputDefaults' => array( 'label' => false ))) ?>
<div class="ls-box-filter ls-form ls-form-inline ls-float-left">
<b class="ls-label-text">Nome do usuario:</b>
<?php echo $this->Form->input('nome') ?>
<br>
<b class="ls-label-text">CPF do usuario:</b>
<?php echo $this->Form->input('cpf', array(	'data-mask'  =>	"999.999.999-99")) ?>
<br>
<b class="ls-label-text">Email do usuario:</b>
<?php echo $this->Form->input('email') ?>
<br>
<b class="ls-label-text">Departamento:</b>
<br>
<label class="ls-label col-md-8 col-sm-8 ">
<div class="ls-custom-select">
<?php echo $this->Form->select(
    'departamento_id',
     $departamentos,
    ['empty' => 'Departamento','class' => "ls-select"] 
);?>
</div>
</label>

<br>
<br>
<br>
<b class="ls-label-text">Senha:</b>
<?php echo $this->Form->input('senha', array('type' => 'password')) ?>
<br>
	  <b class="ls-label-text">Imagem de Perfil:</b>
<?php echo $this->Form->input('img', array( 'type' => 'file')); ?>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<hr>
<button class="ls-btn ">Criar</button>

<?php echo $this->Form->end(); ?>
