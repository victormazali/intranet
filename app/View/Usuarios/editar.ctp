<?php if( $canAdmin == 0 ){ ?>
<form action="" class="ls-form ls-form-horizontal ls-form-disable" data-ls-module="form">
  <label class="ls-label col-md-3">
    <b class="ls-label-text">Departamento</b>
    <input type="text" name="nome" placeholder="<?php echo $user['Departamento']['descricao']?>" class="ls-field" required>
  </label>
 <?php } ?>
</form>
<?php echo $this->Form->create('Usuario', array( 'type' => 'file','inputDefaults' => array( 'label' => false ))) ?>
<?php echo $this->Form->hidden('id') ?>
<form action="" class="ls-form ls-form-row ls-form-disable" data-ls-module="form">
	<label class="ls-label col-md-3">
	    <b class="ls-label-text">CPF:</b>
	    <?php if( $canAdmin == 1 ){ ?>
	<?php echo $this->Form->input('cpf',array(	'data-mask'  =>	"999.999.999-99")) ?>
	<?php } else { ?>
	<?php echo $this->Form->input('cpf',array(	 'disabled' => 'disabled'	)) ?> 
	<?php } ?>
	</label>

	<label class="ls-label col-md-3">
	  <b class="ls-label-text">Nome:</b>
	<?php echo $this->Form->input('nome') ?>
	</label>

	<label class="ls-label col-md-3">
	  <b class="ls-label-text">Email:</b>
	<?php echo $this->Form->input('email') ?>
	</label>

<?php if( $canAdmin == 1 ){ ?>
<label class="ls-label col-md-3">
      <b class="ls-label-text">Departamento</b>
<div class="ls-custom-select">
<?php echo $this->Form->select('departamento_id',$dp,['empty' => 'Departamento','class' => "ls-select"] );?>
</div>
</label>
<?php } ?>

		<label class="ls-label col-md-3">
			  <b class="ls-label-text">Senha:</b>
		<?php echo $this->Form->input('senha', array('type' => 'password', 'value' => '', 'placeholder' => 'Deixe em branco para nao alterar','style'=>'width:250px; ')) ?>
		</label>

<label class="ls-label col-md-3">
	  <b class="ls-label-text">Imagem de Perfil:</b>
<?php echo $this->Form->input('img', array( 'type' => 'file')); ?>
</label>

<br>
<hr>
<button class="ls-btn">Salvar</button>
</form>
<?php echo $this->Form->end(); ?>