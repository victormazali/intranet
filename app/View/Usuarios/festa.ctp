<h1>Lista de Presença </h1>
<br>
<br>

<table class="ls-table ls-table-striped">
	<thead>
		<tr>
			<th>Colaborador</th>
			<th>Acompanhante</th>
		
		</tr>
	</thead>
	<tbody>	
		<?php foreach( $evt as $d ) { ?>
				<td><?php echo $d['Usuario']['nome'] ?></td>
				<td><?php echo $d['Usuario']['acompanhante'] ?></td>
				
			</tr>
		<?php } ?>
	
	</tbody>
</table>
<hr>
<h3>Confirmar Presença</h3>
<?php echo $this->Form->create('Usuario', array( 'inputDefaults' => array( 'label' => false ))) ?>

<br>
<br>


<b class="ls-label-text">Acompanhante:</b>
<?php echo $this->Form->input('acompanhante', array('placeholder' => 'Deixe em branco para nao adicionar acompanhante','style'=>'width:450px; ')) ?>
<br>

<?php echo $this->Form->hidden('festa', array('value' => 1)) ?>

<?php echo $this->Form->hidden($iduser = $this->Session->read('Usuario.id'))  ?>
<button class="ls-btn ">Confirmar</button>
<?php echo $this->Form->end(); ?>


<script type="text/javascript">
$(document).ready( function () {
	$("#dpstatus").change(function() {
		var parametro = $('option:selected', "#dpstatus").attr('filtro');
		document.location.href = '<?php echo $this->webroot ?>'+'Eventos/index/'+parametro;
	});
});
</script>