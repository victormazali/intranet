<header class="ls-info-header">
      <h2 class="ls-title-3"><?php echo $this->Session->read('Usuario.nome')?> => Editar 	Perfil</h2>
</header>

<?php echo $this->Form->create('Usuario', array( 'inputDefaults' => array( 'label' => false ))) ?>

<?php echo $this->Form->hidden('id') ?>

<form action="" class="ls-form ls-form-row ls-form-disable" data-ls-module="form">
  <label class="ls-label col-md-3">
    <b class="ls-label-text">CPF</b>
    <?php echo $this->Form->input('cpf',array(	 'disabled' => 'disabled'	)) ?> 
  </label>

  <label class="ls-label col-md-3">
    <b class="ls-label-text">Nome</b>
    <?php echo $this->Form->input('nome') ?>
  </label>

  <label class="ls-label col-md-3">
    <b class="ls-label-text">Senha</b>
    <?php echo $this->Form->input('senha', array('type' => 'password', 'value' => '', 'placeholder' => 'Deixe em branco para nao alterar','style'=>'width:250px; ')) ?>
  </label>
</form>


<hr>

<button class="ls-btn">Salvar</button>
<?php echo $this->Form->end(); ?>