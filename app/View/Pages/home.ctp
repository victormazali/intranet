<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<div class="container col-md-12 ">

  <header class="ls-info-header ls-txt-center">
      <h2 class="ls-title-3 ls-txt-center">Time Line</h2>
</header>
</div>

<?php if ($postdestaque != NULL) {?>
<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 2px;
                          ">
  <div class="ls-txt-center" style=" height: 60px;">
    <h4 class="ls-title-4 ls-ico-star3">Destaque</h4>
  </div>
  <header class="ls-info-header">
    <img src="<?php echo $this->webroot.$postdestaque['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" />
    <h4 class="ls-title-4"><?php echo $postdestaque['Usuario']['nome'] ?></h4>
    <p class="ls-float-right ls-float-none-xs ls-small-info">
      Postado em <strong><?php echo date('d/m/Y', strtotime($postdestaque['Posthome']['created'])) ?> as <?php echo date('H:i:s', strtotime($postdestaque['Posthome']['created'])) ?></strong>
    </p>
  </header>
  <?php if($postdestaque['Posthome']['usuario_id'] == $this->Session->read('Usuario.id') ) { ?>
<?php echo $this->Form->postLink(
                  '   <button class="ls-btn-primary ls-btn-xs ls-ico-remove ls-float-right"></button>',
                  array('controller' => 'Posthomes','action' => 'excluir', $postdestaque['Posthome']['id']),
                  array('confirm' => 'Deseja excluir o post?', 'escape' => false)); ?>
    <?php } ?>
  <p><?php echo  nl2br(h($postdestaque['Posthome']['conteudo'])) ?></p>
  <?php if($postdestaque['Posthome']['imagem'] != NULL ) { ?>
  <img class="img-responsive" src="<?php echo $this->webroot.$postdestaque['Posthome']['imagem'] ?>" height="300" />
  <?php } ?>
  <br>
<br>
<br>

  <footer class="ls-info-header">
<?php echo $this->Form->create('Coment', array( 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'comentar'))) ?>
 
   <?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Escreva um comentário','style'=>'width:100%;')) ?>
  <?php echo $this->Form->hidden('posthome_id', array( 'value' => $postdestaque['Posthome']['id'] )) ?>
  <p class="ls-float-right ls-float-none-xs ls-small-info">
 <button class="ls-btn-primary ">Comentar</button>
 </p>
<?php echo $this->Form->end(); ?>

  </footer>
    <div id="box-toggle">
      <span class="openComentario">
        <p class="ls-cursor-pointer ls-txt-center ls-color-info">
          Ver Comentarios (<?php echo count($postdestaque['Coment']) ?>)
        </p>
      </span>
      <div class="tgl comentario" style="display:none;">
          <br>
              <?php foreach( $postdestaque['Coment'] as $c)  { ?>
              <div class="ls-box ls-xs-space ls-txt-left ">
              <p class="ls-float-right ls-float-none-xs ls-small-info">
              Postado em <strong><?php echo date('d/m/Y', strtotime($c['created'])) ?> as <?php echo date('H:i:s', strtotime($c['created'])) ?></strong>
              </p>
               
              <h5 class="ls-title-5"> 
              <img src="<?php echo $this->webroot.$c['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" />
             
              <?php echo $c['Usuario']['nome'] ?></h5>

              <p> <?php echo $c['conteudo'] ?> </p>
              </div>
              <?php } ?>
          
      </div>
  </div>
</div>
<?php } ?>
<div class="container col-md-12">
  <hr>
</div>

<?php if($canAdmin == 1) {?>
<?php echo $this->Form->create('Posthome', array( 'type' => 'file','inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'postar'))) ?>
<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 0.5px;
                          ">

<?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Comente algo...','style'=>'width:100%; margin: 3px;')) ?>
<br>
<p class="ls-float-right ls-float-none-xs ls-small-info">
      <button class="ls-btn-primary">Postar</button>
    </p>
  <span class="ls-btn-primary">
 <?php echo $this->Form->input('img', array( 'type' => 'file')); ?>
  </span> 
</div>

<?php echo $this->Form->end(); ?>


<div class="container col-md-6 col-md-offset-3">
  <hr>
</div>
<?php } ?>
<?php foreach( $posts as $p)  { ?>

<div class="ls-box col-md-6 col-md-offset-3" style="
                          background: #fafafa; 
                          border-width: 2px;
                          ">
  <header class="ls-info-header">
      
     <img src="<?php echo $this->webroot.$p['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" />
    <h2 class="ls-title-3"><?php echo $p['Usuario']['nome'] ?></h2>

    <p class="ls-float-right ls-float-none-xs ls-small-info">
      Postado em <strong><?php echo date('d/m/Y', strtotime($p['Posthome']['created'])) ?> as <?php echo date('H:i:s', strtotime($p['Posthome']['created'])) ?></strong>
    </p>
  </header>
<?php if($p['Posthome']['usuario_id'] == $this->Session->read('Usuario.id') ) { ?>
<?php echo $this->Form->postLink(
                  '   <button class="ls-btn-primary ls-btn-xs ls-ico-remove ls-float-right"></button>',
                  array('controller' => 'Posthomes','action' => 'excluir', $p['Posthome']['id']),
                  array('confirm' => 'Deseja excluir o post?', 'escape' => false)); ?>
    <?php } ?>
  <p><?php echo nl2br(h($p['Posthome']['conteudo'])) ?></p>
  <?php if($p['Posthome']['imagem'] != NULL ) { ?>
  <img class="img-responsive" src="<?php echo $this->webroot.$p['Posthome']['imagem'] ?>" height="300" />
  <?php } ?>
<br>
<br>
<br>

  <footer class="ls-info-header">
<?php echo $this->Form->create('Coment', array( 'inputDefaults' => array( 'label' => false ), 'url' => array( 'action' => 'comentar'))) ?>
    <?php echo $this->Form->textarea('conteudo', array('placeholder' => 'Escreva um comentario','style'=>'width:100%;')) ?>
  <?php echo $this->Form->hidden('posthome_id', array( 'value' => $p['Posthome']['id'] )) ?>
  <p class="ls-float-right ls-float-none-xs ls-small-info">
 <button class="ls-btn-primary">Comentar</button>
 </p>
<?php echo $this->Form->end(); ?>

  </footer>
    <div id="box-toggle">
      <span class="openComentario">
        <p class="ls-cursor-pointer ls-txt-center ls-color-info">
          Ver Comentarios (<?php echo count($p['Coment']) ?>)
        </p>
      </span>
      <div class="tgl comentario" style="display:none;">
          <br>
              <?php foreach( $p['Coment'] as $c)  { ?>
              <div class="ls-box ls-xs-space ls-txt-left ">
              <p class="ls-float-right ls-float-none-xs ls-small-info">
              Postado em <strong><?php echo date('d/m/Y', strtotime($c['created'])) ?> as <?php echo date('H:i:s', strtotime($c['created'])) ?></strong>
              </p>
             
              <h5 class="ls-title-5"> 
              
              <img src="<?php echo $this->webroot.$c['Usuario']['imagem'] ?>" style="border-radius: 50%; height: 50px; width: 50px;-webkit-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              -moz-box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);
              box-shadow: 0px 0px 7px -2px rgba(0,0,0,0.75);" />
         
              <?php echo $c['Usuario']['nome'] ?></h5>

              <p> <?php echo $c['conteudo'] ?> </p>
              </div>
              <?php } ?>
          
      </div>
  </div>
</div>
<div class="container col-md-12">
  <hr>
</div>

<?php } ?>

<?php echo $this->Form->end(); ?>

<script type="text/javascript">

$(document).ready(function(){
  $(".openComentario").click(function() {
    $(this).next().slideToggle()();
  });
})

</script>

